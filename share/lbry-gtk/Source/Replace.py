################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################


class Replace:
    Current = ""

    def __init__(self, Widgets, Space):
        self.Widgets, self.Space = Widgets, Space

    def Replace(self, Name):
        if self.Current != Name:
            self.Current = Name
            for Child in self.Space.get_children():
                self.Space.remove(Child)
            Widget = self.Widgets[Name]
            Parent = Widget.get_parent()
            if Parent != None:
                Parent.remove(Widget)
            self.Space.add(Widget)

    def GetSpace(self):
        return self.Current
