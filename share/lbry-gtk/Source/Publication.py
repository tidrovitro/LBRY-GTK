################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, re, queue, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from flbry import url, channel

from Source import Places, Settings, Move, Popup
from Source.Open import Open
from Source.FetchComment import FetchComment
from Source.Thumbnail import Thumbnail
from Source.Document import Document
from Source.Tag import Tag
from Source.PublicationControl import PublicationControl
from Source.Markdown import Markdown
from Source.Comment import Notifies


def GetType(List, Type):
    for Item in List:
        if isinstance(Item, Type):
            return Item


class Publication:
    def __init__(self, *args):
        (
            self.Builder,
            self.ShowHiderTexts,
            self.Window,
            self.Stater,
            self.Title,
            self.MainSpace,
            self.AddPage,
        ) = args

        self.Queue = queue.Queue()

        Builder = self.Builder

        self.Publication = Builder.get_object("Publication")
        self.DataView = Builder.get_object("DataView")
        self.Expanders = [
            Builder.get_object("TagsExpander"),
            Builder.get_object("DescriptionExpander"),
            Builder.get_object("LinksExpander"),
            Builder.get_object("CommentExpander"),
        ]
        self.ThumbnailDataBox = Builder.get_object("ThumbnailDataBox")
        self.DescriptionBox = Builder.get_object("DescriptionBox")
        self.DataStore = Builder.get_object("DataStore")
        self.TagBox = Builder.get_object("TagBox")
        self.LinksBox = Builder.get_object("LinksBox")
        self.PublicationControlBox = Builder.get_object("PublicationControlBox")
        self.DataAction = Builder.get_object("DataAction")
        self.TagsAction = Builder.get_object("TagsAction")
        self.DescriptionAction = Builder.get_object("DescriptionAction")
        self.LinksAction = Builder.get_object("LinksAction")
        self.CommentsAction = Builder.get_object("CommentsAction")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.SelectNext = Builder.get_object("SelectNext")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.CommentChannelOnCurrent = Builder.get_object(
            "CommentChannelOnCurrent"
        )
        self.CommentChannelOnNew = Builder.get_object("CommentChannelOnNew")
        self.CommentEdit = Builder.get_object("CommentEdit")
        self.CommentDelete = Builder.get_object("CommentDelete")
        self.CommentProfile = Builder.get_object("CommentProfile")
        self.CommentUnhide = Builder.get_object("CommentUnhide")
        self.CommentHeart = Builder.get_object("CommentHeart")
        self.CommentLike = Builder.get_object("CommentLike")
        self.CommentDislike = Builder.get_object("CommentDislike")
        self.CommentReply = Builder.get_object("CommentReply")
        self.CommentChannels = Builder.get_object("CommentChannels")
        self.CommentReplyText = Builder.get_object("CommentReplyText")
        self.CommentTip = Builder.get_object("CommentTip")
        self.CommentPost = Builder.get_object("CommentPost")
        self.CommentSave = Builder.get_object("CommentSave")
        self.MarkdownDown = Builder.get_object("MarkdownDown")
        self.MarkdownUp = Builder.get_object("MarkdownUp")
        self.TagSelect = Builder.get_object("TagSelect")
        self.DataView.set_search_equal_func(self.SearchFunction)

        self.Documenter = Document(
            self.Window,
            self.GetPublication,
            self.Stater,
            self.ShowHiderTexts,
            self.Title,
            self.MainSpace,
            self.AddPage,
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Markdown.glade")
        self.Markdowner = Markdown(
            Builder,
            self.Window,
            self.GetPublication,
            "",
            "",
            True,
            True,
            self.AddPage,
        )
        Builder.connect_signals(self.Markdowner)
        self.DescriptionBox.pack_start(self.Markdowner.Document, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Markdown.glade")
        self.Linker = Markdown(
            Builder,
            self.Window,
            self.GetPublication,
            "",
            "",
            True,
            True,
            self.AddPage,
            True,
        )
        Builder.connect_signals(self.Linker)
        self.LinksBox.pack_start(self.Linker.Document, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Thumbnail.glade")
        self.Thumbnailer = Thumbnail(Builder, self.Window)
        Builder.connect_signals(self.Thumbnailer)
        self.ThumbnailDataBox.pack_start(
            self.Thumbnailer.Thumbnail, True, True, 0
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.Tagger = Tag(Builder, False)
        Builder.connect_signals(self.Tagger)
        self.TagBox.pack_start(self.Tagger.Tag, True, True, 0)
        self.Tagger.AddPage = self.AddPage

        self.Commenter = FetchComment(
            self.Window,
            self.ShowHiderTexts,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.Expanders[3],
        )

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "PublicationControl.glade"
        )
        self.Links = []
        self.PublicationControler = PublicationControl(
            Builder,
            self.Window,
            self.Tagger,
            self.GetPublication,
            self.Documenter,
            self.Links,
            self.Stater,
            self.AddPage,
        )
        Builder.connect_signals(self.PublicationControler)
        self.PublicationControlBox.pack_start(
            self.PublicationControler.PublicationControl, True, True, 0
        )

    def ShowLinks(self, Links):
        Text = ""
        del self.Links[:]
        for Link in Links:
            self.Links.append(Link[1])
            Text += "[%s](%s)\n" % (Link[0], Link[1])
        self.Linker.Text = Text
        self.Linker.Fill()

    def GetLinks(self, Url):
        Links = url.web(Url)
        GLib.idle_add(self.ShowLinks, Links)

    def GetTitle(self, Data):
        Title = ""
        try:
            Title = Data["signing_channel"]["value"]["title"] + " - "
        except:
            pass
        try:
            Title = Title + Data["value"]["title"]
        except:
            Title = Title + Data["name"][1:]
        return Title

    def GetPublication(self, Url, Check=False, State=False):
        self.Queue.put(True)

        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Data = url.get([Url], server=Session["Server"])[0]
        if isinstance(Data, str):
            Popup.Error(Data, self.Window)
            return
        NewTitle = self.GetTitle(Data)
        if not State:
            self.Stater.Save(self.GetPublication, [Url], NewTitle)
        GLib.idle_add(self.ShowPublication, Data)

    def ShowPublication(self, Data):
        self.Replace("Publication")
        for Expander in self.Expanders:
            Expander.set_expanded(False)

        NewTitle = self.GetTitle(Data)

        ClaimID = Data["claim_id"]

        try:
            Channel = Data["signing_channel"]["permanent_url"]
            ChannelID = Data["signing_channel"]["claim_id"]
        except:
            Channel = Data["permanent_url"]
            ChannelID = Data["claim_id"]

        self.PublicationControler.ShowPublicationControl(Data, NewTitle)

        self.Title.set_text(NewTitle)

        # This seems to be overly complicated
        Unneeded = ["value_description", "value_tags"]
        Tocheck = [Data]
        TocheckNames = ["data"]
        Branches = {"data": None}
        BranchPlaces = {str(Data): None}
        i = 0
        self.DataStore.clear()
        while i < len(Tocheck):
            Branch = Tocheck[i]
            BranchName = TocheckNames[i]
            if BranchName != "data":
                Branches[BranchName] = self.DataStore.append(
                    BranchPlaces[str(Branch)], (str(BranchName), "")
                )
            if isinstance(Branch, dict):
                Iter = Branch.items()
            else:
                Iter = enumerate(Branch)
            for Key, Value in Iter:
                if isinstance(Value, dict) or isinstance(Value, list):
                    BranchPlaces[str(Value)] = Branches[BranchName]
                    TocheckNames.append(Key)
                    Tocheck.append(Value)
                else:
                    if not (str(BranchName) + "_" + str(Key)) in Unneeded:
                        self.DataStore.append(
                            Branches[BranchName], (str(Key), str(Value))
                        )
            i += 1

        try:
            self.Markdowner.Text = str(Data["value"]["description"])
        except:
            self.Markdowner.Text = "No description"

        self.Markdowner.Fill()

        self.Tagger.RemoveAll()
        try:
            self.Tagger.Append(Data["value"]["tags"])
        except:
            pass

        TagFlowChildren = len(self.Tagger.Tags)
        if TagFlowChildren == 0:
            self.Expanders[0].set_label("No Tags")
            self.Tagger.Add("I told you")
        else:
            self.Expanders[0].set_label("Tags (" + str(TagFlowChildren) + ")")

        try:
            self.Thumbnailer.Url = Data["value"]["thumbnail"]["url"]
        except:
            self.Thumbnailer.Url = ""

        Args = [Data["canonical_url"]]
        threading.Thread(None, self.GetLinks, None, Args).start()

        del self.ShowHiderTexts[:]
        self.Publication.show_all()
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Session = LBRYSettings["Session"]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        if LBRYSettings["EnableComments"]:
            ChannelList = channel.channel_list(server=Session["Server"])
            CommentServer = LBRYSettings["CommentServer"]
            self.Expanders[3].show()
            CommentBox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
            self.Commenter.SingleComment(
                ClaimID,
                CommentBox,
                [0, 0, "", "", "", "", "", 0, 0, 0, False, False, False],
                {"Root": CommentBox},
                CommentServer,
                True,
                False,
                LBRYSettings,
                ChannelList,
                Channel,
            )
            self.ActiveComment = CommentBox.get_children()[0]
            self.MakeActive(self.ActiveComment)
            self.Expanders[3].remove(self.Expanders[3].get_children()[0])
            self.Expanders[3].add(CommentBox)
            self.Queue.put(True)
            self.Queue = queue.Queue()
            Args = [
                ClaimID,
                CommentBox,
                self.Queue,
                CommentServer,
                Channel,
            ]
            threading.Thread(
                None, self.Commenter.Fetch, None, Args, daemon=True
            ).start()
            self.Expanders[3].set_label("Comments")
            Args = [self.Expanders[3], ChannelID, CommentServer]
            threading.Thread(
                None, self.Commenter.DisplayPrice, None, Args
            ).start()
        else:
            self.Expanders[3].hide()

        self.Markdown = [self.Markdowner.SelectNext]
        self.Markdown.append(self.Markdowner.SelectPrevious)
        self.Link = [self.Linker.SelectNext, self.Linker.SelectPrevious]

    def Fraction(self, Children, Value):
        for Child in Children:
            if isinstance(Child, Gtk.ProgressBar):
                Child.set_fraction(Value)
                break

    def MakeActive(self, Comment=False):
        if not Comment:
            return
        Children = self.ActiveComment.get_children()[0].get_children()
        self.Fraction(Children, 0)
        Children = Comment.get_children()[0].get_children()
        self.Fraction(Children, 1)
        self.ActiveComment = Comment

    def SearchFunction(self, Model, Column, Key, Iter):
        Row = Model[Iter]
        if Key.lower() in list(Row)[Column].lower():
            return False

        for Inner in Row.iterchildren():
            if Key.lower() in list(Inner)[Column].lower():
                self.DataView.expand_to_path(Row.path)
                break
        else:
            self.DataView.collapse_row(Row.path)
        return True

    def ChangeExpander(self, Index):
        for ExpanderIndex in range(len(self.Expanders)):
            self.Expanders[ExpanderIndex].set_expanded(ExpanderIndex == Index)

    def on_DataAction_activate(self, Widget):
        self.ChangeExpander(4)
        self.DataView.get_selection().select_path(0)
        self.DataView.grab_focus()

    def on_TagsAction_activate(self, Widget):
        self.ChangeExpander(0)
        self.Tagger.FlowBox.get_children()[0].grab_focus()

    def on_DescriptionAction_activate(self, Widget):
        self.ChangeExpander(1)

    def on_LinksAction_activate(self, Widget):
        self.ChangeExpander(2)

    def on_CommentsAction_activate(self, Widget):
        self.ChangeExpander(3)

    def on_MoveLeft_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxLeftRight(self.Tagger.FlowBox, -1)
        elif self.Expanders[3].get_expanded():
            Parent = self.ActiveComment.get_parent().get_parent()
            if isinstance(Parent, Gtk.Box):
                Expander = Parent.get_parent()
                self.MakeActive(Expander.get_parent().get_parent().get_parent())
                Expander.set_expanded(False)
        elif self.DataView.get_realized():
            self.SelectDataUpdate(self.DataExitUpdate)

    def DataExitUpdate(self):
        try:
            Selection = self.DataView.get_selection()
            Path = Selection.get_selected_rows()[1][0]
            Path.up()
            Selection.select_path(Path)
            self.DataView.collapse_row(Path)
        except:
            pass

    def on_MoveRight_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxLeftRight(self.Tagger.FlowBox)
        elif self.Expanders[3].get_expanded():
            Children = self.ActiveComment.get_children()[0].get_children()
            Expander = GetType(Children, Gtk.Frame).get_children()[0]
            Box = GetType(Expander.get_children(), Gtk.Box)
            SubComments = GetType(Box.get_children(), Gtk.Box).get_children()
            if len(SubComments) != 0:
                self.MakeActive(SubComments[0])
                Expander.set_expanded(True)
        elif self.DataView.get_realized():
            Selection = self.DataView.get_selection()
            Path = Selection.get_selected_rows()[1][0]
            if self.DataView.expand_row(Path, False):
                Path.append_index(0)
                Selection.select_path(Path)
            else:
                self.SelectDataUpdate()

    def GetChild(self, Change):
        Box = self.ActiveComment.get_parent()
        Children = Box.get_children()
        Position = Box.child_get_property(self.ActiveComment, "position")
        if Position + Change < 0:
            return
        for Child in Children:
            if Box.child_get_property(Child, "position") == Position + Change:
                return Child

    def on_MoveDown_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxUpDown(self.Tagger.FlowBox)
        elif self.Expanders[3].get_expanded():
            self.MakeActive(self.GetChild(1))
        elif self.DataView.get_realized():
            Move.ListViewUpDown(self.DataView)
            Path = self.DataView.get_selection().get_selected_rows()[1][0]
            self.DataView.scroll_to_cell(Path, None, False, 0, 0)

    def on_MoveUp_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxUpDown(self.Tagger.FlowBox, -1)
        elif self.Expanders[3].get_expanded():
            self.MakeActive(self.GetChild(-1))
        elif self.DataView.get_realized():
            Move.ListViewUpDown(self.DataView, -1)
            Path = self.DataView.get_selection().get_selected_rows()[1][0]
            self.DataView.scroll_to_cell(Path, None, False, 0, 0)

    def on_OpenOnCurrent_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                self.Tagger.on_Search_button_press_event("", "", 0)
            elif self.Expanders[1].get_expanded():
                self.Markdowner.Activate(0)
            elif self.Expanders[2].get_expanded():
                self.Linker.Activate(0)
            elif self.Expanders[3].get_expanded():
                self.ActiveComment.notify("label")
            else:
                self.PublicationControler.Selector.ActivateUpdate(0)
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.Activate(0)

    def on_OpenOnNew_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                self.Tagger.on_Search_button_press_event("", "", 1)
            elif self.Expanders[1].get_expanded():
                self.Markdowner.Activate(1)
            elif self.Expanders[2].get_expanded():
                self.Linker.Activate(1)
            elif self.Expanders[3].get_expanded():
                self.ActiveComment.notify("label-widget")
            else:
                self.PublicationControler.Selector.ActivateUpdate(1)
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.Activate(1)

    def SelectDataUpdate(self, Function=False):
        try:
            if Function:
                self.DataView.set_cursor(Gtk.TreePath.new(), None, True)
            else:
                Path = self.DataView.get_selection().get_selected_rows()[1][0]
                Column = self.DataView.get_column(1)
                self.DataView.set_cursor(Path, Column, True)
        except:
            pass
        if Function:
            Function()

    def on_TagSelect_activate(self, Widget):
        if self.Publication.get_realized() and self.Expanders[0].get_expanded():
            self.Tagger.Select()

    def on_SelectNext_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[3].get_expanded():
            self.ActiveComment.notify("name")
        else:
            self.PublicationControler.Selector.Forward()

    def on_SelectPrevious_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[3].get_expanded():
            self.ActiveComment.notify("style")
        else:
            self.PublicationControler.Selector.Backward()

    def on_CommentAction_activate(self, Widget):
        Index = int(Widget.get_label())
        if self.Publication.get_realized():
            if 14 < Index:
                if self.Expanders[1].get_expanded():
                    self.Markdown[Index - 15]()
                    return
                elif self.Expanders[2].get_expanded():
                    self.Link[Index - 15]()
                    return
            self.ActiveComment.get_children()[0].notify(Notifies[Index])
        elif self.Documenter.Markdowner.Document.get_realized():
            if Index == 15:
                self.Documenter.Markdowner.SelectNext()
            elif Index == 16:
                self.Documenter.Markdowner.SelectPrevious()
