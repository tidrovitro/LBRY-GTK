################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import meta

from Source import Places, Settings, KeyBind, Popup


def GetTopPanel(Window):
    TopPanelParent = Window.get_children()[0].get_children()[1]
    Children = TopPanelParent.get_children()
    for Child in Children:
        if Child.get_name() == "TopPanelBox":
            return Child.get_children()[0]


def UpdateWidth(Window, Session):
    Pager = Window.get_children()[0].get_children()[0]
    Length = Pager.get_n_pages()
    for Index in range(Length):
        Page = Pager.get_nth_page(Index)
        Label = Pager.get_tab_label(Page)
        Children = Label.get_children()
        Children[0].set_size_request(Session["PageWidth"], -1)
        Children[1].set_hexpand(Session["PageExpand"])


UpdateMetaQueue = queue.Queue()


def UpdateMeta(Inbox, Window):
    # Exit if user disabled Meta Service otherwise continue
    global UpdateMetaQueue
    threading.Thread(None, UpdateMetaThread, None, [Inbox, Window]).start()
    try:
        UpdateMetaQueue.get(block=False)
        return False
    except:
        return True


def UpdateMetaThread(Inbox, Window):
    # Check if user disabled Meta Service
    global UpdateMetaQueue
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    if not Session["EnableMetaService"]:
        UpdateMetaQueue.put(True)
        return

    # Get Inbox
    LBRYSettings = Settings.Get()
    if isinstance(LBRYSettings, str):
        Popup.Error(LBRYSettings, Window)
        return
    LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"]["LBRY-GTK"]

    try:
        Notification = meta.list(
            LBRYGTKSettings["MetaServer"], LBRYGTKSettings["AuthToken"], True
        )[0]

        # Check if last notification is new
        with open(Places.CacheDir + "Notification.json", "r") as File:
            LastNotification = json.load(File)["LastNotification"]
        if LastNotification != Notification["id"]:
            GLib.idle_add(UpdateMetaUpdate, Inbox)
    except:
        pass


def UpdateMetaUpdate(Inbox):
    Inbox.set_active(True)


def ChangeMeta(Window, EnableMetaService):
    # Hide or show Icon
    TopPanel = GetTopPanel(Window)
    Children = TopPanel.get_children()
    for Child in Children:
        try:
            if Child.get_children()[0].get_label() == "🔔":
                Inbox = Child
                Child.set_visible(EnableMetaService)
                break
        except:
            pass

    # Start notification service
    if EnableMetaService:
        GLib.timeout_add(60 * 1000, UpdateMeta, Inbox, Window)
        threading.Thread(None, UpdateMetaThread, None, [Inbox, Window]).start()


MinimumWidth = 0

MenuIcons = [
    "menu_new",
    "preferences-other",
    "image-loading",
    "document-properties",
    "applications-others",
]


def ChangeMenu(Window, Type, MenuIcon="", Remove=True):
    # Set MinimumWidth with full menu only once
    global MinimumWidth, MenuIcons
    if MinimumWidth == 0:
        MinimumWidth = Window.get_preferred_size().minimum_size.width

    TopPanel = GetTopPanel(Window)
    Children = TopPanel.get_children()
    Menu = ""

    # Get all needed components
    for Child in Children:
        if isinstance(Child, Gtk.Box):
            try:
                Menu = Child.get_children()[0]
            except:
                pass
            MenuBox = Child
        if isinstance(Child, Gtk.MenuButton):
            Button = Child
    Popover = Button.get_popover()
    if Menu == "":
        Menu = Popover.get_children()[0]

    # Set icon
    Image = Button.get_children()[0]
    if MenuIcon != "" and Image.get_icon_name() != MenuIcons[MenuIcon]:
        Image.set_from_icon_name(MenuIcons[MenuIcon], Gtk.IconSize.BUTTON)

    # Do not change type if it's already that
    if TopPanel.get_name() != str(Type):
        TopPanel.set_name(str(Type))

        # Initial settings needed for each type
        Menu.unparent()
        MenuBox.foreach(MenuBox.remove)
        Popover.foreach(Popover.remove)
        Button.set_visible(Type == 2)
        WindowName = Window.get_name()

        # Do not remove function, if original type is adaptive
        if WindowName != "GtkWindow" and Remove:
            Window.disconnect(int(WindowName))
            Window.set_name("GtkWindow")
        # Add menu to the correct place
        # Adaptive adds it to the button, as function assumes it is somewhere
        if Type == 0:
            Window.set_name(str(Window.connect("configure-event", UpdateMenu)))
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)
            UpdateMenu(Window, "", Window.get_size().width)
        elif Type == 1:
            Menu.set_pack_direction(Gtk.PackDirection.LTR)
            MenuBox.add(Menu)
        elif Type == 2:
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)


def UpdateMenu(Window, Event, Width=""):
    # Setting MenuType depending on Window size
    if Width == "":
        Width = Event.width
    global MinimumWidth
    if Width < MinimumWidth:
        ChangeMenu(Window, 2, "", False)
    else:
        ChangeMenu(Window, 1, "", False)


def GetWindowWidgets(Main):
    return {
        "KBRefresh": [Main.RefreshAction.activate],
        "KBAdd": [Main.AddAction.activate],
        "KBBackOnCurrent": [Main.BackOnCurrent.activate],
        "KBBackOnNew": [Main.BackOnNew.activate],
        "KBForwardOnCurrent": [Main.ForwardOnCurrent.activate],
        "KBForwardOnNew": [Main.ForwardOnNew.activate],
        "KBLBRY-GTK": [Main.LBRY_GTK.activate],
        "KBSearch": [Main.Search.activate],
        "KBInboxOnCurrent": [Main.InboxOnCurrent.activate],
        "KBInboxOnNew": [Main.InboxOnNew.activate],
        "KBHomeOnCurrent": [Main.HomeOnCurrent.activate],
        "KBHomeOnNew": [Main.HomeOnNew.activate],
        "KBFollowingOnCurrent": [Main.FollowingOnCurrent.activate],
        "KBFollowingOnNew": [Main.FollowingOnNew.activate],
        "KBYourTagsOnCurrent": [Main.YourTagsOnCurrent.activate],
        "KBYourTagsOnNew": [Main.YourTagsOnNew.activate],
        "KBDiscoverOnCurrent": [Main.DiscoverOnCurrent.activate],
        "KBDiscoverOnNew": [Main.DiscoverOnNew.activate],
        "KBLibraryOnCurrent": [Main.LibraryOnCurrent.activate],
        "KBLibraryOnNew": [Main.LibraryOnNew.activate],
        "KBCollectionsOnCurrent": [Main.CollectionsOnCurrent.activate],
        "KBCollectionsOnNew": [Main.CollectionsOnNew.activate],
        "KBFollowedOnCurrent": [Main.FollowedOnCurrent.activate],
        "KBFollowedOnNew": [Main.FollowedOnNew.activate],
        "KBUploadsOnCurrent": [Main.UploadsOnCurrent.activate],
        "KBUploadsOnNew": [Main.UploadsOnNew.activate],
        "KBChannelsOnCurrent": [Main.ChannelsOnCurrent.activate],
        "KBChannelsOnNew": [Main.ChannelsOnNew.activate],
        "KBPreviousPage": [Main.Previous.activate],
        "KBNextPage": [Main.Next.activate],
        "KBMenu": [Main.TopPaneler.ShowMenu.activate],
        "KBNewPublicationOnCurrent": [
            Main.TopPaneler.NewPublicationOnCurrent.activate
        ],
        "KBNewPublicationOnNew": [Main.TopPaneler.NewPublicationOnNew.activate],
        "KBSettingsOnCurrent": [Main.TopPaneler.SettingsOnCurrent.activate],
        "KBSettingsOnNew": [Main.TopPaneler.SettingsOnNew.activate],
        "KBHelpOnCurrent": [Main.TopPaneler.HelpOnCurrent.activate],
        "KBHelpOnNew": [Main.TopPaneler.HelpOnNew.activate],
        "KBStatusOnCurrent": [Main.TopPaneler.StatusOnCurrent.activate],
        "KBStatusOnNew": [Main.TopPaneler.StatusOnNew.activate],
        "KBAbout": [Main.TopPaneler.AboutAction.activate],
        "KBBalanceOnCurrent": [Main.TopPaneler.BalanceOnCurrent.activate],
        "KBBalanceOnNew": [Main.TopPaneler.BalanceOnNew.activate],
        "KBMainOnCurrent": [Main.OnCurrent.activate],
        "KBMainOnNew": [Main.OnNew.activate],
        "KBMainSelectPrevious": [Main.SelectPrevious.activate],
        "KBMainSelectNext": [Main.SelectNext.activate],
        "KBBackHistory": [Main.BackHistory.activate],
        "KBForwardHistory": [Main.ForwardHistory.activate],
    }


def GetPageWidgets(Page):
    Publicationer = Page.Publicationer
    PublicationControler = Publicationer.PublicationControler
    return {
        "KBClose": [Page.CloseButton.activate],
        "KBScrollDown": [Page.ScrollDown.activate],
        "KBScrollUp": [Page.ScrollUp.activate],
        "KBMoveLeft": [Page.MoveLeft.activate, Publicationer.MoveLeft.activate],
        "KBMoveRight": [
            Page.MoveRight.activate,
            Publicationer.MoveRight.activate,
        ],
        "KBMoveUp": [Page.MoveUp.activate, Publicationer.MoveUp.activate],
        "KBMoveDown": [Page.MoveDown.activate, Publicationer.MoveDown.activate],
        "KBPageOnCurrent": [
            Page.ListOpenOnCurrent.activate,
            Publicationer.OpenOnCurrent.activate,
        ],
        "KBPageOnNew": [
            Page.ListOpenOnNew.activate,
            Publicationer.OpenOnNew.activate,
        ],
        "KBPageSelectNext": [Publicationer.SelectNext.activate],
        "KBPageSelectPrevious": [Publicationer.SelectPrevious.activate],
        "KBPlayOnCurrent": [PublicationControler.PlayOnCurrent.activate],
        "KBPlayOnNew": [PublicationControler.PlayOnNew.activate],
        "KBContentOnCurrent": [PublicationControler.ContentOnCurrent.activate],
        "KBContentOnNew": [PublicationControler.ContentOnNew.activate],
        "KBChannelOnCurrent": [PublicationControler.ChannelOnCurrent.activate],
        "KBChannelOnNew": [PublicationControler.ChannelOnNew.activate],
        "KBRSS": [PublicationControler.RSSAction.activate],
        "KBFollow": [PublicationControler.UnFollowAction.activate],
        "KBDownload": [PublicationControler.DownloadAction.activate],
        "KBLink": [PublicationControler.LinkAction.activate],
        "KBRepost": [PublicationControler.RepostAction.activate],
        "KBRelatedOnCurrent": [PublicationControler.RelatedOnCurrent.activate],
        "KBRelatedOnNew": [PublicationControler.RelatedOnNew.activate],
        "KBBoost": [PublicationControler.BoostAction.activate],
        "KBTip": [PublicationControler.TipAction.activate],
        "KBCommentChannelOnCurrent": [
            Publicationer.CommentChannelOnCurrent.activate
        ],
        "KBCommentChannelOnNew": [Publicationer.CommentChannelOnNew.activate],
        "KBCommentEdit": [Publicationer.CommentEdit.activate],
        "KBCommentDelete": [Publicationer.CommentDelete.activate],
        "KBCommentProfile": [Publicationer.CommentProfile.activate],
        "KBCommentUnhide": [Publicationer.CommentUnhide.activate],
        "KBCommentHeart": [Publicationer.CommentHeart.activate],
        "KBCommentLike": [Publicationer.CommentLike.activate],
        "KBCommentDislike": [Publicationer.CommentDislike.activate],
        "KBCommentReply": [Publicationer.CommentReply.activate],
        "KBCommentChannels": [Publicationer.CommentChannels.activate],
        "KBCommentReplyText": [Publicationer.CommentReplyText.activate],
        "KBCommentTip": [Publicationer.CommentTip.activate],
        "KBCommentPost": [Publicationer.CommentPost.activate],
        "KBCommentSave": [Publicationer.CommentSave.activate],
        "KBMarkdownDown": [Publicationer.MarkdownDown.activate],
        "KBMarkdownUp": [Publicationer.MarkdownUp.activate],
        "KBTagSelect": [Publicationer.TagSelect.activate],
        "KBThumbnail": [Publicationer.Thumbnailer.ThumbnailOpen.activate],
        "KBData": [Publicationer.DataAction.activate],
        "KBTags": [Publicationer.TagsAction.activate],
        "KBDescription": [Publicationer.DescriptionAction.activate],
        "KBLinks": [Publicationer.LinksAction.activate],
        "KBComments": [Publicationer.CommentsAction.activate],
    }


def GetMenuWidgets(Main):
    return {
        "KBMainOnCurrent": [Main.OnCurrent.activate],
        "KBMainOnNew": [Main.OnNew.activate],
        "KBMainSelectPrevious": [Main.SelectPrevious.activate],
        "KBMainSelectNext": [Main.SelectNext.activate],
        "KBMenu": [Main.TopPaneler.ShowMenu.activate],
    }


def ShortCut(AccelGroup, Window, KeyVal, Modifier, Function):
    Function()


def CreateLambda(Function):
    return lambda A, B, C, D: ShortCut(A, B, C, D, Function)


def AcceleratorCreate(Window, Widgets, AccelGroup):
    Window.add_accel_group(AccelGroup)
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    for WidgetName in Widgets:
        try:
            for Widget in Widgets[WidgetName]:
                Converted = KeyBind.Convert(Session[WidgetName])
                if Converted:
                    AccelGroup.connect(*Converted, 0, CreateLambda(Widget))
        except:
            pass


WindowAccelGroup = ""


def WindowAcceleratorCreate(Window, Main):
    global WindowAccelGroup
    Widgets = GetWindowWidgets(Main)
    if WindowAccelGroup != "":
        Window.remove_accel_group(WindowAccelGroup)
    WindowAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, Widgets, WindowAccelGroup)


PageAccelGroup = ""


def PageAcceleratorCreate(Window, Page):
    global PageAccelGroup
    Widgets = GetPageWidgets(Page)
    if PageAccelGroup != "":
        Window.remove_accel_group(PageAccelGroup)
    PageAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, Widgets, PageAccelGroup)


def MenuAcceleratorCreate(Window, Main):
    AcceleratorCreate(Window, GetMenuWidgets(Main), Gtk.AccelGroup.new())
    Window.set_name("Set")
