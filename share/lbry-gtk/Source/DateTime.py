################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import datetime, calendar, time


class DateTime:
    def __init__(self, Builder):
        self.Builder = Builder
        self.Date = self.Builder.get_object("Date")
        self.Hour = self.Builder.get_object("Hour")
        self.Minute = self.Builder.get_object("Minute")
        self.Second = self.Builder.get_object("Second")
        self.DateTime = self.Builder.get_object("DateTime")
        self.DateUse = self.Builder.get_object("DateUse")
        self.on_DateReset_clicked()

    def Set(self, Time):
        self.Date.select_month(Time.month - 1, Time.year)
        self.Date.select_day(Time.day)
        self.OldHour, self.OldMinute, self.OldSecond = (
            Time.hour,
            Time.minute,
            Time.second,
        )
        self.Hour.set_value(Time.hour)
        self.Minute.set_value(Time.minute)
        self.Second.set_value(Time.second)

    def on_DateReset_clicked(self, Widget=""):
        self.Set(datetime.datetime.now())

    def on_Second_value_changed(self, Widget):
        self.OlderSecond = self.OldSecond
        self.OldSecond = Widget.get_value()

    def on_Minute_value_changed(self, Widget):
        self.OlderMinute = self.OldMinute
        self.OldMinute = Widget.get_value()

    def on_Hour_value_changed(self, Widget):
        self.OlderHour = self.OldHour
        self.OldHour = Widget.get_value()

    def on_Second_wrapped(self, Widget):
        if self.OldSecond < self.OlderSecond:
            self.Minute.spin(Gtk.SpinType.STEP_FORWARD, 1)
        else:
            self.Minute.spin(Gtk.SpinType.STEP_BACKWARD, 1)

    def on_Minute_wrapped(self, Widget):
        if self.OldMinute < self.OlderMinute:
            self.Hour.spin(Gtk.SpinType.STEP_FORWARD, 1)
        else:
            self.Hour.spin(Gtk.SpinType.STEP_BACKWARD, 1)

    def on_Hour_wrapped(self, Widget):
        Date = self.Date.get_date()
        self.Date.select_day(1)
        Day, Month, Year = Date.day, Date.month, Date.year
        if self.OldHour < self.OlderHour:
            Days = calendar.monthrange(Year, Month + 1)[1]
            if Days < Day + 1:
                if 11 < Month + 1:
                    Month = -1
                    Year += 1
                self.Date.select_month(Month + 1, Year)
                Day = 0
            self.Date.select_day(Day + 1)
        else:
            if Day - 1 < 1:
                if Month - 1 < 0:
                    Month = 12
                    Year -= 1
                self.Date.select_month(Month - 1, Year)
                Day = calendar.monthrange(Year, Month)[1] + 1
            self.Date.select_day(Day - 1)

    def GetTime(self):
        Time = -1
        if self.DateUse.get_active():
            Date = self.Date.get_date()
            Hour = int(self.Hour.get_value())
            Minute = int(self.Minute.get_value())
            Second = int(self.Second.get_value())
            Date = datetime.datetime(
                Date.year, Date.month + 1, Date.day, Hour, Minute, Second
            )
            Time = int(time.mktime(Date.timetuple()))
        return Time

    def SetTime(self, TimeStamp):
        self.Set(datetime.datetime.fromtimestamp(TimeStamp))
