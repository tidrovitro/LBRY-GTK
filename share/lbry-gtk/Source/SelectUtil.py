################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


def Event(Widget, Button):
    Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
    Event.button = Button
    Event.window = Widget.get_window()
    Widget.event(Event)


def SendEvent(Function, Widget, Button):
    Event(Widget, Types[Button])
    Function()


def Functions(AllFunctions):
    AllFunctions = [*AllFunctions]
    for Function in AllFunctions:
        Function()


def FunctionsLambda(*AllFunctions):
    return lambda Argument="": Functions(AllFunctions)


def EventLambda(MenuItem, Function):
    return lambda Button: SendEvent(Function, MenuItem, Button)
