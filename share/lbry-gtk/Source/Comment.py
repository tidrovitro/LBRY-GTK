################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from flbry import error

from Source import Image, Places, Timestamp, Popup
from Source.Open import Open
from Source.Channels import Channels
from Source.Markdown import Markdown
from Source.CommentControlTop import CommentControlTop
from Source.CommentControlMiddle import CommentControlMiddle
from Source.CommentControlBottom import CommentControlBottom
from Source.Thumbnail import Thumbnail
from Source.Select import Select

Notifies = ["parent", "events", "valign", "halign", "margin", "name", "vexpand"]
Notifies.extend(["style", "resize-mode", "spacing", "hexpand", "window"])
Notifies.extend(["visible", "opacity", "is-focus", "sensitive", "homogeneous"])
Messages = [[0, 0], [1, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6]]
Messages.extend([[0, 7], [1, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13]])
Promo = "\n\nSent from [*LBRY-GTK*](https://codeberg.org/MorsMortium/LBRY-GTK)"


class Comment:
    def __init__(self, *args):
        (
            self.Builder,
            self.ShowHiderTexts,
            self.ClaimID,
            self.Box,
            self.Row,
            self.Boxes,
            self.Window,
            self.SingleComment,
            self.GetPublication,
            HiddenUI,
            Start,
            self.CommentServer,
            self.Settings,
            self.ChannelList,
            self.Stater,
            self.AddPage,
            PChannel,
            self.CommentExpander,
        ) = args

        self.Comment = self.Builder.get_object("Comment")
        self.CommentTextBox = self.Builder.get_object("CommentTextBox")
        self.HideDislikedBox = self.Builder.get_object("HideDislikedBox")
        self.RepliesFrame = self.Builder.get_object("RepliesFrame")
        self.ReplySeparator = self.Builder.get_object("ReplySeparator")
        self.RepliesExpander = self.Builder.get_object("RepliesExpander")
        self.RepliesBox = self.Builder.get_object("RepliesBox")
        self.ProfileBox = self.Builder.get_object("ProfileBox")
        self.PreviewBox = self.Builder.get_object("PreviewBox")
        self.CCTopBox = self.Builder.get_object("CommentControlTopBox")
        self.CCMiddleBox = self.Builder.get_object("CommentControlMiddleBox")
        self.CCBottomBox = self.Builder.get_object("CommentControlBottomBox")
        self.ReplyScrolled = self.Builder.get_object("ReplyScrolled")
        self.ReplyText = self.Builder.get_object("ReplyText")
        self.UnHideButton = self.Builder.get_object("UnHideButton")
        self.ProfileActive = self.Builder.get_object("ProfileActive")
        self.CommentActive = self.Builder.get_object("CommentActive")
        self.PreviewActive = self.Builder.get_object("PreviewActive")

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Channels.glade")
        self.Channelser = Channels(
            Builder,
            False,
            self.Window,
            self.ChannelList,
            self.Settings,
        )
        Builder.connect_signals(self.Channelser)
        self.Channels = self.Channelser.Channels

        self.Markdowns = [{"Text": self.Row[3], "Box": self.CommentTextBox}]
        self.Markdowns.append({"Text": "", "Box": self.PreviewBox})

        # The comment is being checked here whether it should
        # be hidden or not
        for MarkdownItem in self.Markdowns:
            if self.Row[3] == MarkdownItem["Text"]:
                LikeBoost = self.Row[8] * 5
                if not LikeBoost:
                    LikeBoost = 5
                if LikeBoost <= self.Row[9]:
                    if self.Settings["HideDisliked"] == True:
                        continue
                self.HideDislikedBox.set_no_show_all(True)
            self.MarkdownItems(MarkdownItem)

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlBottom.glade"
        )

        self.CCBottomer = CommentControlBottom(
            Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Markdowns,
            self.Box,
            self.ClaimID,
            self.CommentServer,
            self.ChannelList,
            self.SingleComment,
            self.Boxes,
            self.Settings,
            self.RepliesFrame,
            self.RepliesBox,
            self.RepliesExpander,
            HiddenUI,
            PChannel,
            self.ReplyText,
            Promo,
            self.on_ReplyText_key_release_event,
            self.CommentExpander,
        )
        Builder.connect_signals(self.CCBottomer)
        self.CCBottomBox.pack_start(
            self.CCBottomer.CommentControlBottom, True, True, 0
        )
        self.ChannelsBox2 = self.CCBottomer.ChannelsBox2
        self.Post = self.CCBottomer.Post
        self.Save = self.CCBottomer.Save
        self.Tip = self.CCBottomer.Tip
        self.TipLabel = self.CCBottomer.TipLabel

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlTop.glade"
        )
        self.CCToper = CommentControlTop(
            Builder,
            self.Window,
            self.Row,
            self.GetPublication,
            self.ChannelList,
            self.CommentServer,
            self.Box,
            self.ShowHiderTexts,
            self.Markdowns,
            self.Comment,
            self.ReplyText,
            self.ReplyScrolled,
            self.PreviewBox,
            self.Save,
            self.on_ReplyText_key_release_event,
            self.Stater,
            self.AddPage,
        )
        Builder.connect_signals(self.CCToper)
        self.CCTopBox.pack_start(self.CCToper.CommentControlTop, True, True, 0)
        self.Channel = self.CCToper.Channel
        self.Support = self.CCToper.Support
        self.CommentDate = self.CCToper.CommentDate
        self.Edit = self.CCToper.Edit
        self.Delete = self.CCToper.Delete

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlMiddle.glade"
        )
        self.CCMiddler = CommentControlMiddle(
            Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel,
            PChannel,
            self.ChannelList,
            self.CCToper.EditImage,
        )

        Builder.connect_signals(self.CCMiddler)
        self.CCMiddleBox.pack_start(
            self.CCMiddler.CommentControlMiddle, True, True, 0
        )

        self.Heart = self.CCMiddler.Heart
        self.Like = self.CCMiddler.Like
        self.Dislike = self.CCMiddler.Dislike
        self.LikeNumber = self.CCMiddler.LikeNumber
        self.DislikeNumber = self.CCMiddler.DislikeNumber
        self.Reply = self.CCMiddler.Reply
        self.ChannelsBox1 = self.CCMiddler.ChannelsBox1

        self.CCBottomer.on_Edit_button_press_event = (
            self.CCToper.on_Edit_button_press_event
        )
        self.CCBottomer.Edit = self.Edit
        self.CCBottomer.on_Reply_button_press_event = (
            self.CCMiddler.on_Reply_button_press_event
        )
        self.CCBottomer.Reply = self.Reply

        self.ChannelsParent = self.ChannelsBox1
        if HiddenUI:
            self.ChannelsParent = self.ChannelsBox2

        Widgets = [self.Channel, self.Edit, self.Delete, self.ProfileBox]
        Widgets.extend([self.UnHideButton, self.CommentTextBox])
        Widgets.extend([self.Heart.get_children()[0]])
        Widgets.extend([self.Like.get_children()[0]])
        Widgets.extend([self.Dislike.get_children()[0]])
        Widgets.extend([self.Reply, self.ChannelsParent, self.ReplyText])
        Widgets.extend([self.Tip, self.Post, self.Save, self.PreviewBox])

        Exceptions = [self.Tip, self.ProfileBox, self.ReplyText]
        Exceptions.extend([self.CommentTextBox, self.PreviewBox])
        Exceptions.extend([self.ChannelsParent])

        Enters = [self.Tip.grab_focus, self.ProfileMax]
        Enters.extend([self.ReplyText.grab_focus, self.CommentMax])
        Enters.extend([self.PreviewMax, self.ChannelsMax])

        Leaves = [self.TipUnselect, self.ProfileMin, self.Comment.grab_focus]
        Leaves.extend([self.CommentMin, self.PreviewMin, self.ChannelsMin])

        Activates = [self.Tip.grab_focus, self.ReplyText.grab_focus]
        try:
            Activates.extend([self.Markdowns[0]["Markdowner"].Activate])
        except:
            Activates.extend([""])
        Activates.extend([self.Markdowns[1]["Markdowner"].Activate])
        Activates.extend([self.Channels.popup])

        if self.Settings["Profile"]:
            Builder = Gtk.Builder.new_from_file(
                Places.GladeDir + "Thumbnail.glade"
            )
            self.Thumbnailer = Thumbnail(
                Builder,
                self.Window,
                self.Settings["ProfileWidth"],
                self.Settings["ProfileHeight"],
            )
            Builder.connect_signals(self.Thumbnailer)
            self.ProfileBox.pack_start(
                self.Thumbnailer.Thumbnail, True, True, 0
            )
            self.Thumbnailer.Url = self.Row[6]
            self.ProfileBox.set_no_show_all(False)
            self.ProfileActive.set_no_show_all(False)
            Activates.insert(1, self.Thumbnailer.on_ThumbnailOpen_activate)

        self.Selector = Select(Widgets, Exceptions, Enters, Leaves, Activates)

        ReplyIndent = int(self.Settings["ReplyIndent"])
        ReplySeparatorWidth = int(self.Settings["ReplySeparator"])

        self.RepliesFrame.set_margin_start(ReplyIndent)
        self.ReplySeparator.set_size_request(ReplySeparatorWidth, 0)

        for Channel in self.ChannelList:
            if Channel[3] == PChannel:
                self.Heart.set_no_show_all(False)
                self.Heart.set_sensitive(True)
                break

        if self.Row[12]:
            self.Heart.set_no_show_all(False)

        if HiddenUI:
            self.ChannelsBox2.add(self.Channels)
            self.ReplyScrolled.set_visible(True)
            self.Post.set_visible(True)
            self.PreviewBox.get_parent().set_visible(True)
            self.Tip.set_visible(True)
            self.TipLabel.set_visible(True)
            ToHide = [self.Channel, self.Support, self.CommentDate, self.Heart]
            ToHide.extend([self.Like, self.Dislike, self.Delete, self.Edit])
            ToHide.extend([self.CommentTextBox, self.HideDislikedBox])
            ToHide.extend([self.Reply, self.ProfileBox, self.ProfileActive])
            ToHide.extend([self.Save, self.LikeNumber, self.DislikeNumber])
            ToHide.extend([self.CommentActive, self.ChannelsBox1])
            for Widget in ToHide:
                Widget.set_no_show_all(True)
                Widget.hide()
        else:
            self.ChannelsBox2.set_no_show_all(True)
            self.ChannelsBox2.hide()
            self.ChannelsBox1.add(self.Channels)

        if Start:
            self.Box.reorder_child(self.Comment, 0)

        self.Channel.set_label(self.Row[2])
        self.Support.set_label(str(self.Row[1]) + " LBC")

        TimeAgo = Timestamp.Ago(self.Row[7])
        self.CommentDate.set_label(TimeAgo)
        DateText = Timestamp.GetDate(self.Row[7])
        self.CommentDate.set_tooltip_text(DateText)

        self.ShowHiderTexts.append(self.ReplyText)
        if self.Row[2] != "":
            for Channel in self.ChannelList:
                if Channel[0] == self.Row[2]:
                    self.Edit.set_no_show_all(False)
                    self.Delete.set_no_show_all(False)
                    break
        if self.Row[0] != 0:
            self.RepliesFrame.set_no_show_all(False)
            self.Boxes[self.Row[4]] = self.RepliesBox
            self.RepliesExpander.set_label("Replies (" + str(self.Row[0]) + ")")
        self.Box.add(self.Comment)
        self.Box.show_all()
        self.MarkdownCommands = [self.MarkdownDown, self.MarkdownUp]

    def ProfileMax(self):
        self.ProfileActive.set_value(1)

    def ProfileMin(self):
        self.ProfileActive.set_value(0)

    def ChannelsMax(self):
        self.ChannelsParent.get_children()[1].set_value(1)

    def ChannelsMin(self):
        self.ChannelsParent.get_children()[1].set_value(0)

    def CommentMax(self):
        self.CommentActive.set_value(1)

    def CommentMin(self):
        self.CommentActive.set_value(0)

    def PreviewMax(self):
        self.PreviewActive.set_value(1)

    def PreviewMin(self):
        self.PreviewActive.set_value(0)

    def TipUnselect(self):
        self.Tip.select_region(0, 0)

    def ChannelsActivate(self, Discard=""):
        self.Channels.popup()

    def TipActivate(self, Discard=""):
        self.Tip.grab_focus()

    def ReplyTextActivate(self, Discard=""):
        self.ReplyText.grab_focus()

    def GetMarkdown(self):
        if self.Selector.CurrentItem == 5:
            return self.Markdowns[0]["Markdowner"]
        if self.Selector.CurrentItem == 15:
            return self.Markdowns[1]["Markdowner"]
        return False

    def MarkdownDown(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectNext()

    def MarkdownUp(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectPrevious()

    def on_Comment_notify(self, Widget, Property):
        if Property.name == "name":
            self.Selector.Forward()
        elif Property.name == "style":
            self.Selector.Backward()
        elif Property.name == "label":
            self.Selector.ActivateUpdate(0)
        elif Property.name == "label-widget":
            self.Selector.ActivateUpdate(1)

    def on_Box_notify(self, Widget, Property):
        if Property.name == "has-tooltip":
            return
        Index = Notifies.index(Property.name)
        if 14 < Index:
            self.MarkdownCommands[Index - 15]()
            return
        self.Selector.ActivateUpdate(*Messages[Index])

    def MarkdownItems(self, MarkdownItem):
        MarkdownItem["MarkdownBuilder"] = Gtk.Builder.new_from_file(
            Places.GladeDir + "Markdown.glade"
        )
        MarkdownItem["Markdowner"] = Markdown(
            MarkdownItem["MarkdownBuilder"],
            self.Window,
            self.GetPublication,
            MarkdownItem["Text"],
            "",
            True,
            self.Settings["EnableMarkdown"],
            self.AddPage,
        )
        MarkdownItem["MarkdownBuilder"].connect_signals(
            MarkdownItem["Markdowner"]
        )
        MarkdownItem["Box"].pack_start(
            MarkdownItem["Markdowner"].Document, True, True, 0
        )
        self.ShowHiderTexts.append(MarkdownItem["Markdowner"].TextBox)
        MarkdownItem["Markdowner"].Fill()

    def on_ReplyText_draw(self, Widget, Discard=""):
        self.ReplyScrolled.get_vadjustment().set_value(0)

    def on_ReplyText_key_release_event(self, Widget, Discard=""):
        TextBuffer = Widget.get_buffer()
        Start = TextBuffer.get_start_iter()
        End = TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(Start, End, False)
        if (
            self.Settings["PromoteLBRYGTK"]
            and Text != ""
            and not self.Save.get_visible()
        ):
            Text += Promo
        self.Markdowns[1]["Text"] = Text
        self.Markdowns[1]["Markdowner"].Text = self.Markdowns[1]["Text"]
        self.Markdowns[1]["Markdowner"].Fill()
        self.Box.show_all()

    def on_UnHideButton_button_press_event(self, Widget, Discard=""):
        BoxWidgets = self.HideDislikedBox.get_children()
        for Items in BoxWidgets:
            self.HideDislikedBox.remove(Items)
        self.MarkdownItems(self.Markdowns[0])

    def on_ProfileBox_draw(self, Widget, Cairo):
        self.Thumbnailer.Thumbnail.queue_draw()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
