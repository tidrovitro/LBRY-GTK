################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib

from flbry import comments

from Source import Places, Popup


class CommentControlMiddle:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel,
            self.PChannel,
            self.ChannelList,
            self.EditImage,
        ) = args

        self.CommentID = self.Row[4]
        self.CommentControlMiddle = self.Builder.get_object(
            "CommentControlMiddle"
        )
        self.Heart = self.Builder.get_object("Heart")
        self.Like = self.Builder.get_object("Like")
        self.Dislike = self.Builder.get_object("Dislike")
        self.LikeNumber = self.Builder.get_object("LikeNumber")
        self.DislikeNumber = self.Builder.get_object("DislikeNumber")
        self.Reply = self.Builder.get_object("Reply")
        self.ChannelsBox1 = self.Builder.get_object("ChannelsBox1")

        self.Like.set_active(self.Row[10])
        self.Dislike.set_active(self.Row[11])
        self.Heart.set_active(self.Row[12])

        self.LikeNumber.set_label(" %s " % str(self.Row[8]))
        self.DislikeNumber.set_label(" %s " % str(self.Row[9]))

    def React(self, Channel, Type, Remove):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.reaction_react(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            Type,
            Remove,
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Popup.Error(ErrorOrData, self.Window)
            return ""
        ErrorOrData = comments.reaction_list(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Popup.Error(ErrorOrData, self.Window)
            return ""
        return ErrorOrData

    def on_Heart_button_press_event(self, Widget, Event=""):
        Remove = [Widget.get_active()]
        threading.Thread(None, self.HeartThread, None, Remove).start()

    def on_Parent_button_press_event(self, Widget, Event=""):
        Widget.get_parent().event(Event)

    def HeartThread(self, Remove):
        Channel = []
        for ChannelItem in self.ChannelList:
            if ChannelItem[3] == self.PChannel:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break
        if Channel == []:
            return
        Type = "creator_like"
        ErrorOrData = self.React(Channel, Type, Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.LikeDislikeUpdate, Channel, ErrorOrData)

    def on_Dislike_button_press_event(self, Widget, Event=""):
        self.LikeDislike("dislike", Widget.get_active())

    def on_Like_button_press_event(self, Widget, Event=""):
        self.LikeDislike("like", Widget.get_active())

    def LikeDislike(self, Type, Active):
        ChannelRow = self.Channelser.Get()
        Channel = [ChannelRow[0], ChannelRow[-1]]
        args = [Channel, Type, Active]
        threading.Thread(None, self.LikeDislikeThread, None, args).start()

    def LikeDislikeThread(self, Channel, Type, Remove):
        ErrorOrData = self.React(Channel, Type, Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.LikeDislikeUpdate, Channel, ErrorOrData)

    def LikeDislikeUpdate(self, Channel, Data):
        MyLike = Data["my_reactions"][self.CommentID]["like"]
        MyDislike = Data["my_reactions"][self.CommentID]["dislike"]
        OtherLike = Data["others_reactions"][self.CommentID]["like"]
        OtherDislike = Data["others_reactions"][self.CommentID]["dislike"]
        Heart = Data["my_reactions"][self.CommentID]["creator_like"]
        Heart += Data["others_reactions"][self.CommentID]["creator_like"]
        self.Like.set_active(MyLike == 1)
        self.Dislike.set_active(MyDislike == 1)
        self.LikeNumber.set_label(" %s " % str(OtherLike + MyLike))
        self.DislikeNumber.set_label(" %s " % str(OtherDislike + MyDislike))
        self.Heart.set_active(Heart == 1)

    def on_Reply_button_press_event(self, Widget, Event=""):
        if self.Edit.get_image() == self.EditImage:
            Visible = self.ReplyScrolled.get_visible()
            self.ReplyScrolled.set_visible(not Visible)
            self.Post.set_visible(not Visible)
            self.PreviewBox.get_parent().set_visible(not Visible)
            self.Tip.set_visible(not Visible)
            self.TipLabel.set_visible(not Visible)

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
