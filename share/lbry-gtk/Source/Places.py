################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import appdirs, os, sys, tempfile

AppName = "LBRY-GTK"
AppAuthor = "MorsMortium"

if sys.platform != "win32":
    Root = os.path.abspath(os.path.dirname(os.path.dirname(sys.argv[0])))
else:
    Root = os.path.abspath(os.path.dirname(sys.argv[0]))

ShareDir = os.path.join(Root, "share", "")

LBRYGTKDir = os.path.join(ShareDir, "lbry-gtk", "")

ImageDir = os.path.join(ShareDir, "icons", "hicolor", "scalable", "apps", "")

TmpDir = os.path.join(tempfile.gettempdir(), "LBRY-GTK", "")

HelpDir = os.path.join(LBRYGTKDir, "Help", "")

GladeDir = os.path.join(LBRYGTKDir, "Glade", "")

JsonDir = os.path.join(LBRYGTKDir, "Json", "")

ConfigDir = os.path.join(appdirs.user_config_dir(AppName, AppAuthor), "")

CacheDir = os.path.join(appdirs.user_cache_dir(AppName, AppAuthor), "")
