################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from Source import Places


def Helper(Text, Window):
    Dialog = Gtk.MessageDialog(
        Window,
        Gtk.DialogFlags.DESTROY_WITH_PARENT,
        Gtk.MessageType.INFO,
        Gtk.ButtonsType.CLOSE,
        Text,
    )

    Dialog.get_message_area().get_children()[0].set_selectable(True)

    Dialog.run()
    Dialog.destroy()


def Error(Text, Window):
    DeveloperMode = False
    try:
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        DeveloperMode = Session["DeveloperMode"]
    except:
        pass
    if DeveloperMode:
        GLib.idle_add(Helper, Text, Window)


def Message(Text, Window):
    GLib.idle_add(Helper, Text, Window)
