################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango, Gdk

from Source.TextColumn import TextColumn
from Source import Timestamp


class Box:
    def __init__(self, *args):
        (
            self.Builder,
            self.Pixbuf,
            self.Row,
            self.Publicationer,
            TitleRows,
            AuthorRows,
            Padding,
            HiddenExtra,
            self.AddPage,
            self.Stater,
        ) = args

        self.Box = self.Builder.get_object("Box")

        if isinstance(self.Row[3], type(None)):
            self.Row[3] = ""
        if isinstance(self.Row[4], type(None)):
            self.Row[4] = ""

        ToolTip = self.Row[3]
        if ToolTip != "" and self.Row[4] != "":
            ToolTip += " - "
        ToolTip += self.Row[4]

        self.Box.set_tooltip_text(ToolTip)

        Grid = self.Box.get_child()
        Grid.set_margin_start(Padding)
        Grid.set_margin_end(Padding)
        Grid.set_margin_top(Padding)
        Grid.set_margin_bottom(Padding)

        Width = self.Pixbuf.get_width()

        Title = TextColumn(self.Row[4], Width, TitleRows, True, True)
        self.Builder.get_object("TitleBox").add(Title.Column)

        Author = TextColumn(self.Row[3], Width, AuthorRows, True, True)
        self.Builder.get_object("AuthorBox").add(Author.Column)

        self.Confirmations = self.Builder.get_object("Confirmations")
        self.ConfirmationsLabel = self.Builder.get_object("ConfirmationsLabel")
        self.Amount = self.Builder.get_object("Amount")
        self.AmountLabel = self.Builder.get_object("AmountLabel")
        self.IsTip = self.Builder.get_object("IsTip")

        if HiddenExtra:
            self.Confirmations.set_no_show_all(True)
            self.ConfirmationsLabel.set_no_show_all(True)
            self.Amount.set_no_show_all(True)
            self.AmountLabel.set_no_show_all(True)
            self.IsTip.set_no_show_all(True)
            self.IsTip.set_visible(False)
        else:
            self.Confirmations.set_label(str(self.Row[0]))
            self.Amount.set_label(str(self.Row[1]))
            self.IsTip.set_active(self.Row[2])
        Time = Timestamp.Ago(self.Row[5])
        Tooltip = Timestamp.GetDate(self.Row[5])
        self.Builder.get_object("Time").set_label(Time)
        self.Builder.get_object("Time").set_tooltip_text(Tooltip)
        self.Builder.get_object("Type").set_label(self.Row[6])

    def on_Box_button_press_event(self, Widget, Event):
        if Event.button != 8 and Event.button != 9:
            Url = [self.Row[-3]]
            if Url != [""]:
                if Event.button == Gdk.BUTTON_PRIMARY:
                    threading.Thread(
                        None, self.Publicationer.GetPublication, None, Url
                    ).start()
                elif Event.button == Gdk.BUTTON_MIDDLE:
                    self.AddPage(
                        ".",
                        "",
                        self.Stater.Export(
                            self.Publicationer.GetPublication, Url
                        ),
                    )

    def on_Box_enter_notify_event(self, Widget, Discard=""):
        FlowChild = Widget.get_parent()
        FlowChild.get_parent().select_child(FlowChild)

    def on_Box_leave_notify_event(self, Widget, Discard=""):
        Widget.get_parent().get_parent().unselect_all()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
