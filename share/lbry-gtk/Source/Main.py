################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, setproctitle, json, threading, sys

setproctitle.setproctitle("LBRY-GTK")

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk

from flbry import connect
from Source import Places, Settings, SettingsUpdate, Popup, SelectUtil
from Source.Page import Page
from Source.TopPanel import TopPanel
from Source.SidePanel import SidePanel
from Source.Select import Select

if sys.platform == "win32":
    MouseBack = 4
    MouseForward = 5
else:
    MouseBack = 8
    MouseForward = 9


class Handler:
    WindowHeight, WindowWidth = 0, 0

    def __init__(self, Builder):
        self.Builder, self.Pages, self.Menu = Builder, [], ""
        self.Window = Builder.get_object("Window")
        self.Pager = Builder.get_object("Pager")
        self.Header = Builder.get_object("Header")
        self.TopPanelBox = Builder.get_object("TopPanelBox")
        self.SidePanelBox = Builder.get_object("SidePanelBox")
        self.Back = Builder.get_object("Back")
        self.Forward = Builder.get_object("Forward")
        self.Add = Builder.get_object("Add")
        self.Refresh = Builder.get_object("Refresh")
        self.Previous = Builder.get_object("Previous")
        self.Next = Builder.get_object("Next")
        self.OnCurrent = Builder.get_object("OnCurrent")
        self.OnNew = Builder.get_object("OnNew")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.BackImage = Builder.get_object("BackImage")

        self.RefreshAction = Builder.get_object("RefreshAction")
        self.AddAction = Builder.get_object("AddAction")
        self.BackOnCurrent = Builder.get_object("BackOnCurrent")
        self.BackOnNew = Builder.get_object("BackOnNew")
        self.ForwardOnCurrent = Builder.get_object("ForwardOnCurrent")
        self.ForwardOnNew = Builder.get_object("ForwardOnNew")
        self.LBRY_GTK = Builder.get_object("LBRY-GTK")
        self.Search = Builder.get_object("Search")
        self.InboxOnCurrent = Builder.get_object("InboxOnCurrent")
        self.InboxOnNew = Builder.get_object("InboxOnNew")
        self.HomeOnCurrent = Builder.get_object("HomeOnCurrent")
        self.HomeOnNew = Builder.get_object("HomeOnNew")
        self.FollowingOnCurrent = Builder.get_object("FollowingOnCurrent")
        self.FollowingOnNew = Builder.get_object("FollowingOnNew")
        self.YourTagsOnCurrent = Builder.get_object("YourTagsOnCurrent")
        self.YourTagsOnNew = Builder.get_object("YourTagsOnNew")
        self.DiscoverOnCurrent = Builder.get_object("DiscoverOnCurrent")
        self.DiscoverOnNew = Builder.get_object("DiscoverOnNew")
        self.LibraryOnCurrent = Builder.get_object("LibraryOnCurrent")
        self.LibraryOnNew = Builder.get_object("LibraryOnNew")
        self.CollectionsOnCurrent = Builder.get_object("CollectionsOnCurrent")
        self.CollectionsOnNew = Builder.get_object("CollectionsOnNew")
        self.FollowedOnCurrent = Builder.get_object("FollowedOnCurrent")
        self.FollowedOnNew = Builder.get_object("FollowedOnNew")
        self.UploadsOnCurrent = Builder.get_object("UploadsOnCurrent")
        self.UploadsOnNew = Builder.get_object("UploadsOnNew")
        self.ChannelsOnCurrent = Builder.get_object("ChannelsOnCurrent")
        self.ChannelsOnNew = Builder.get_object("ChannelsOnNew")
        self.BackHistory = Builder.get_object("BackHistory")
        self.ForwardHistory = Builder.get_object("ForwardHistory")

        self.Window.drag_dest_set(
            Gtk.DestDefaults.ALL,
            [Gtk.TargetEntry.new("text/plain", Gtk.TargetFlags.OTHER_APP, 0)],
            Gdk.DragAction.COPY,
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "TopPanel.glade")

        with open(Places.ImageDir + "lbry-gtk.svg", "r") as File:
            Svg = File.read()
        Loader = GdkPixbuf.PixbufLoader()
        Loader.write(Svg.encode())
        Loader.close()
        self.Logo = Loader.get_pixbuf()

        self.TopPaneler = TopPanel(
            Builder,
            self.Window,
            self.Logo,
            self.BackImage,
            self.NewPage,
            self.SamePage,
            self.SetMenu,
        )
        Builder.connect_signals(self.TopPaneler)
        self.TopPanelBox.pack_start(self.TopPaneler.TopPanel, True, True, 0)
        self.MenuSelector = self.TopPaneler.Selector

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "SidePanel.glade")
        self.SidePaneler = SidePanel(
            Builder,
            self.Window,
            self.NewPage,
            self.SamePage,
        )
        Builder.connect_signals(self.SidePaneler)
        self.SidePanelBox.pack_start(self.SidePaneler.SidePanel, True, True, 0)

        CssProvider = Gtk.CssProvider.new()
        CssProvider.load_from_data(b"* {padding: 0px;}")
        StyleContext = self.Header.get_style_context()
        StyleContext.add_provider(
            CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.on_Add_button_press_event()
        self.ExitQueue = self.Pages[-1].ExitQueue
        self.on_Header_switch_page(self.Header, self.Pages[-1].Page, 0)
        threading.Thread(None, self.Pages[0].Startuper.StartupHelper).start()

        self.TopPaneler.Startuper = self.Pages[0].Startuper
        self.SidePaneler.Startuper = self.Pages[0].Startuper

        self.Window.set_icon_from_file(Places.ImageDir + "lbry-gtk.svg")

        SettingsUpdate.WindowAcceleratorCreate(self.Window, self)

        SidePaneler, TopPaneler = self.SidePaneler, self.TopPaneler
        Widgets = [self.Refresh, self.Add, self.Back, self.Forward]
        Widgets.extend([TopPaneler.LBRY, TopPaneler.Search, TopPaneler.Inbox])
        Widgets.extend([SidePaneler.Home, SidePaneler.Following])
        Widgets.extend([SidePaneler.YourTags, SidePaneler.Discover])
        Widgets.extend([SidePaneler.Library, SidePaneler.Collections])
        Widgets.extend([SidePaneler.Followed, SidePaneler.Uploads])
        Widgets.extend([SidePaneler.Channels])
        Exceptions = [self.TopPaneler.Search]
        Enters = [self.EnterSearch]
        Leaves = [self.TopPaneler.TopPanel.grab_focus]
        Activates = [self.EnterSearch]

        self.Selector = Select(Widgets, Exceptions, Enters, Leaves, Activates)
        self.BackAndForward = [self.Back, self.Forward]

    def EnterSearch(self, Button=""):
        self.TopPaneler.Search.grab_focus()

    def LeftClick(self, Widget):
        self.Selector.ActivateUpdate(0, int(Widget.get_label()))

    def MiddleClick(self, Widget):
        self.Selector.ActivateUpdate(1, int(Widget.get_label()))

    def on_Previous_activate(self, Widget):
        Current = self.Header.get_current_page()
        if Current != 0:
            self.Header.set_current_page(Current - 1)

    def on_Next_activate(self, Widget):
        Current = self.Header.get_current_page()
        if Current != self.Header.get_n_pages() - 1:
            self.Header.set_current_page(Current + 1)

    def NewPage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        State = Page.Stater.Export(Function, Data)
        self.on_Add_button_press_event(".", "", State)

    def SamePage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        Page.Stater.Import(Function, Data)

    def on_MenuItem_button_press_event(self, Widget, Event, State):
        Page = self.Pages[self.Pager.get_current_page()]
        if Event.button == Gdk.BUTTON_MIDDLE:
            self.on_Add_button_press_event(
                ".", "", Page.Stater.Export(State["Function"], State["Data"])
            )
        else:
            Page.Stater.Goto(State["Index"])

    def on_Window_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        if (
            Event.button == MouseBack or Event.button == MouseForward
        ) and Page.Startuper.Started:
            LBRYSettings = Settings.Get()["preferences"]["shared"]["value"]
            if isinstance(LBRYSettings, str):
                Popup.Error(LBRYSettings, self.Window)
                return
            if LBRYSettings["LBRY-GTK"]["MouseBackAndForward"]:
                if Event.button == MouseBack:
                    self.Selector.ActivateUpdate(0, 2)
                elif Event.button == MouseForward:
                    self.Selector.ActivateUpdate(0, 3)

    def on_Window_drag_data_received(
        self, widget, Context, X, Y, Data, Info, Time
    ):
        Page = self.Pages[self.Pager.get_current_page()]
        if Page.Startuper.Started:
            threading.Thread(
                None, Page.Publicationer.GetPublication, None, [Data.get_text()]
            ).start()
        else:
            Popup.Message("LBRYNet is not running.", self.Window)

    def on_StaterList_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        IsBack = Widget.get_image() == self.BackImage
        if Event.button == Gdk.BUTTON_SECONDARY:
            if IsBack:
                List = Page.Stater.Before()
            else:
                List = Page.Stater.After()
            Length = len(List)
            if Length != 0:
                Menu = Gtk.Menu.new()
                Widgets, Exceptions, Enters, Leaves = [], [], [], []
                Activates = []
                for Index in range(Length):
                    Item = List[Index]
                    MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                    Widgets.append(MenuItem)
                    Exceptions.append(MenuItem)
                    Enters.append(MenuItem.select)
                    Leaves.append(MenuItem.deselect)
                    Activates.append(
                        SelectUtil.EventLambda(MenuItem, Menu.popdown)
                    )
                    MenuItem.connect(
                        "button-press-event",
                        self.on_MenuItem_button_press_event,
                        Item,
                    )
                    if IsBack:
                        Start, End = Length - Index - 1, Length - Index
                    else:
                        Start, End = Index, Index + 1
                    Menu.attach(MenuItem, 0, 1, Start, End)
                Menu.show_all()
                Menu.popup_at_widget(
                    Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
                )
                self.MenuSelector = Select(
                    Widgets, Exceptions, Enters, Leaves, Activates
                )
                SettingsUpdate.MenuAcceleratorCreate(Menu.get_parent(), self)
                self.Menu = Menu
        elif Event.button == Gdk.BUTTON_MIDDLE:
            if IsBack:
                State = Page.Stater.Previous()
            else:
                State = Page.Stater.Next()
            if State != "":
                self.on_Add_button_press_event(".", "", State)
        elif Event.button == Gdk.BUTTON_PRIMARY:
            if IsBack:
                threading.Thread(None, Page.Stater.Back).start()
            else:
                threading.Thread(None, Page.Stater.Forward).start()

    def SetMenu(self, Remove=False):
        if Remove:
            self.Menu = ""
            return
        Windows = Gtk.Window.list_toplevels()
        for Window in Windows:
            if (
                Window.get_realized()
                and isinstance(Window.get_child(), Gtk.Menu)
                and Window.get_name() != "Set"
            ):
                Window.connect(
                    "focus-out-event",
                    self.TopPaneler.on_MenuBar_focus_out_event,
                )
                SettingsUpdate.MenuAcceleratorCreate(Window, self)
        self.Menu = self.TopPaneler.MenuBar

    def on_History_activate(self, Widget):
        Button = self.BackAndForward[int(Widget.get_label())]
        SelectUtil.Event(Button, Gdk.BUTTON_SECONDARY)

    def on_Add_button_press_event(self, Widget="", Event="", State=""):
        if Widget == "" or self.Pages[0].Startuper.Started:
            Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Page.glade")
            FirstPage = State
            try:
                with open(Places.ConfigDir + "Session.json", "r") as File:
                    Session = json.load(File)
            except:
                pass

            try:
                LBRYSettings = Settings.Get()
                if (
                    not isinstance(LBRYSettings, str)
                    and Widget != ""
                    and State == ""
                ):
                    Shared = LBRYSettings["preferences"]["shared"]["value"]
                    FirstPage = [
                        Shared["LBRY-GTK"]["HomeFunction"],
                        Shared["LBRY-GTK"]["HomeData"],
                    ]
            except:
                pass

            NoStartup = True
            if Widget == "":
                NoStartup = False

            CloseButton = Gtk.Button.new_from_icon_name(
                "window-close", Gtk.IconSize.BUTTON
            )

            self.Pages.append(
                Page(
                    Builder,
                    self.Window,
                    FirstPage,
                    NoStartup,
                    self.on_Add_button_press_event,
                    self.TopPaneler.Balance,
                    CloseButton,
                    self,
                )
            )
            Builder.connect_signals(self.Pages[-1])

            TitleBox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
            Width = 128
            try:
                Width = Session["PageWidth"]
            except:
                pass
            self.Pages[-1].Title.set_size_request(Width, -1)
            TitleBox.pack_start(self.Pages[-1].Title, False, False, 0)
            self.Pages[-1].Title.connect(
                "draw",
                self.on_Title_draw,
                self.Pages[-1],
            )

            CloseButton.connect(
                "clicked",
                self.on_CloseButton_clicked,
                self.Pages[-1],
            )
            TitleBox.pack_start(CloseButton, True, True, 0)
            CloseButton.set_halign(Gtk.Align.END)
            Expand = True
            try:
                Expand = Session["PageExpand"]
            except:
                pass
            CloseButton.set_hexpand(Expand)
            TitleBox.show_all()
            self.Pager.append_page(self.Pages[-1].MainSpace)
            self.Header.append_page(
                Gtk.Box.new(Gtk.Orientation.VERTICAL, 0), TitleBox
            )
            self.Pager.set_current_page(-1)
            self.Header.show_all()
            self.Header.set_current_page(-1)

    def on_Window_configure_event(self, Widget, Event):
        if self.WindowHeight != Event.height or self.WindowWidth != Event.width:
            self.Pages[self.Pager.get_current_page()].ShowHider.Hide()
            self.WindowHeight = Event.height
            self.WindowWidth = Event.width

    def on_Refresh_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        Page.Stater.Goto(Page.Stater.CurrentState)

    def on_Title_draw(self, Widget, Discard, Page):
        if Widget.get_name() != Widget.get_text():
            Widget.set_name(Widget.get_text())
            self.on_Header_switch_page(
                self.Header, Page, self.Pages.index(Page)
            )

    def on_CloseButton_clicked(self, Widget, Page):
        if len(self.Pages) != 1:
            Page.Publicationer.Queue.put(True)
            self.Header.remove_page(self.Pager.page_num(Page.MainSpace))
            self.Pager.remove_page(self.Pager.page_num(Page.MainSpace))
            self.Pages.remove(Page)

    def on_Header_switch_page(self, Widget, Page, Index):
        Text = (
            Widget.get_tab_label(Widget.get_nth_page(Index))
            .get_children()[0]
            .get_text()
        )
        if Text == "":
            Text = "LBRY-GTK"
        else:
            Text += " - LBRY-GTK"
        self.Window.set_title(Text)
        self.Pager.set_current_page(Index)
        SettingsUpdate.PageAcceleratorCreate(self.Window, self.Pages[Index])

    def on_Window_destroy(self, *args):
        for Page in self.Pages:
            Page.Publicationer.Queue.put(True)
        self.ExitQueue.put(True)
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        if Session["Stop"]:
            connect.stop(server=Session["Server"])
        Gtk.main_quit()
        sys.exit()

    def on_OnCurrent_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.ActivateUpdate(0)
        else:
            self.Selector.ActivateUpdate(0)

    def on_OnNew_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.ActivateUpdate(1)
        else:
            self.Selector.ActivateUpdate(1)

    def on_SelectPrevious_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Backward()
        else:
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Forward()
        else:
            self.Selector.Forward()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")


App = Gtk.Builder.new_from_file(Places.GladeDir + "LBRY-GTK.glade")

App.connect_signals(Handler(App))

Gtk.main()

"""
if " " in command:
    plugin.manager(command[command.find(" ")+1:])
else:
    plugin.manager()
wallet.addresses()
wallet.address_send()
donate.check_devs_file(save_changes=True)
donate.check_devs_file(user_check=True)
donate.check_devs_file(user_check=True, diff=True)
donate.add()
donate.donate()
analytics.sales()
if " " in command:
    plugin.get_plugin(command[command.find(" ")+1:])
else:
    plugin.get_plugin()
command = plugin.run(command, command, "main")
"""
