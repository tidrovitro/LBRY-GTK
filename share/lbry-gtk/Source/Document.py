################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, os, time

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, Gdk


from Source import Places
from Source.Open import Open
from Source.Markdown import Markdown


class Document:
    def __init__(self, *args):
        (
            self.Window,
            self.GetPublication,
            self.Stater,
            self.ShowHiderTexts,
            self.Title,
            MainSpace,
            self.AddPage,
        ) = args
        self.MarkdownBuilder = Gtk.Builder.new_from_file(
            Places.GladeDir + "Markdown.glade"
        )
        self.Markdowner = Markdown(
            self.MarkdownBuilder,
            self.Window,
            self.GetPublication,
            "",
            "",
            False,
            True,
            self.AddPage,
        )
        self.Markdowner.Adjustment = MainSpace.get_vadjustment()
        self.MarkdownBuilder.connect_signals(self.Markdowner)

    def Display(self, Path, Name, State=False):
        self.ShowHiderTexts.append(self.Markdowner.Document)
        if not State:
            self.Stater.Save(self.Display, [Path, Name], "Document: " + Name)
        Size = os.path.getsize(Path)
        Counter = 0
        while Counter < 10:
            time.sleep(0.1)
            if Size == os.path.getsize(Path):
                Counter += 1
            else:
                Counter = 0
                Size == os.path.getsize(Path)
        if not Path.endswith(".md"):
            Open(Path)
            return
        with open(Path, "r", encoding="utf8") as File:
            Text = File.read()

        if "/" in Path:
            Place = Path[::-1].find("/")
        else:
            Place = Path[::-1].find("\\")

        self.Markdowner.Path = Path[: len(Path) - Place]
        self.Markdowner.Text = Text
        GLib.idle_add(self.DisplayHelper, Text, Name)

    def DisplayHelper(self, Text, Name):
        self.Title.set_text("Document: " + Name)
        self.Markdowner.Fill()
        self.Replace("Document")
