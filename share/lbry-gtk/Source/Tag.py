################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class Tag:
    def __init__(self, Builder, Edit):
        self.Builder, self.Edit, self.Tags = Builder, Edit, []
        self.Entry = self.Builder.get_object("Entry")
        self.FlowBox = self.Builder.get_object("FlowBox")
        self.Tag = self.Builder.get_object("Tag")
        if self.Edit:
            self.Builder.get_object("Search").hide()
        else:
            self.Entry.hide()
            self.Builder.get_object("Add").hide()
            self.Builder.get_object("Remove").hide()

    def Add(self, Text):
        if Text != "" and not Text in self.Tags:
            Tag = Gtk.CheckButton.new_with_label(Text)
            Label = Tag.get_child()
            Label.set_line_wrap(True)
            Label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
            Tag.connect("enter-notify-event", self.on_Tag_enter_notify_event)
            Tag.connect("leave-notify-event", self.on_Tag_leave_notify_event)
            self.FlowBox.add(Tag)
            self.Entry.set_text("")
            self.FlowBox.show_all()
            self.Tags.append(Text)

    def on_Tag_enter_notify_event(self, Widget, Discard=""):
        FlowChild = Widget.get_parent()
        FlowChild.get_parent().select_child(FlowChild)

    def on_Tag_leave_notify_event(self, Widget, Discard=""):
        Widget.get_parent().get_parent().unselect_all()

    def Append(self, List):
        for Text in List:
            self.Add(Text)

    def on_Add_clicked(self, Widget=""):
        self.Add(self.Entry.get_text())

    def on_Entry_key_press_event(self, Widget, Event):
        if Gdk.keyval_name(Event.keyval) == "Return":
            self.on_Add_clicked()

    def on_FlowBox_key_press_event(self, Widget, Event):
        Key = Gdk.keyval_name(Event.keyval)
        if self.Edit and (Key == "Delete" or Key == "BackSpace"):
            self.on_Remove_clicked()

    def on_Remove_clicked(self, Widget=""):
        Children = self.FlowBox.get_children()
        for Child in Children:
            if Child.get_child().get_active():
                self.Tags.remove(Child.get_child().get_label())
                self.FlowBox.remove(Child)

    def ChangeAll(self, State=True):
        Children = self.FlowBox.get_children()
        for Child in Children:
            Child.get_child().set_active(State)

    def on_SelectAll_clicked(self, Widget=""):
        self.ChangeAll()

    def on_UnselectAll_clicked(self, Widget=""):
        self.ChangeAll(False)

    def Select(self):
        Child = self.FlowBox.get_selected_children()
        if Child != []:
            Child = Child[0]
            self.on_FlowBox_child_activated("", Child)

    def RemoveAll(self):
        self.on_SelectAll_clicked()
        self.on_Remove_clicked()

    def on_Search_button_press_event(self, Widget, Event="", Button=""):
        if Button == "":
            Button = Event.button
        else:
            Button = Types[Button]
        TagLabels = []
        Children = self.FlowBox.get_children()
        for Child in Children:
            if Child.get_child().get_active():
                TagLabels.append(Child.get_child())
        if len(TagLabels) != 0:
            Tags, NewTitle = [], "Tagsearch:"
            for TagLabel in TagLabels:
                Tags.append(TagLabel.get_child().get_label())
                NewTitle = NewTitle + " " + TagLabel.get_child().get_label()
            Args = ["Search", "Content", NewTitle, {"any_tags": Tags}]
            if Button == Gdk.BUTTON_PRIMARY:
                threading.Thread(None, self.ButtonThread, None, Args).start()
            elif Button == Gdk.BUTTON_MIDDLE:
                self.AddPage(".", "", ["Advanced Search", Args])

    def on_FlowBox_child_activated(self, Widget, Child):
        Child.get_child().set_active((not Child.get_child().get_active()))
