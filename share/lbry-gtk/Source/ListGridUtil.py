################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, math, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from Source import Image, Places, Popup, Timestamp
from Source.Box import Box

from flbry import wallet

CssProvider = Gtk.CssProvider.new()
CssProvider.load_from_data(b"* {padding: 0px;}")


def BalanceHelper(Function, WalletSpaceParts, Window, **kwargs):
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    GotBalance = wallet.balance(server=Session["Server"])
    if isinstance(GotBalance, str):
        Popup.Error(GotBalance, Window)
        return Function(**kwargs)
    Values = [
        GotBalance["total"],
        GotBalance["available"],
        GotBalance["reserved"],
        GotBalance["reserved_subtotals"]["claims"],
        GotBalance["reserved_subtotals"]["tips"],
        GotBalance["reserved_subtotals"]["supports"],
    ]
    for i in range(6):
        Value = float(Values[i])
        WalletSpaceParts[i].set_label(str(math.trunc(Value)))
        WalletSpaceParts[i].set_tooltip_text(str(Value))
    return Function(**kwargs)


def InboxHelper(Function, **kwargs):
    Page, PageSize = kwargs["page"], kwargs["page_size"]
    del kwargs["page"], kwargs["page_size"], kwargs["server"]
    Data = Function(**kwargs)
    if isinstance(Data, str):
        return Data
    else:
        return Data[(Page - 1) * PageSize : Page * PageSize]


def ListUpdate(Row, EmptyCopy, MainSpace, List, Store):
    ToolTip = ""
    Extra = []
    Offset = 3
    if List != "Wallet":
        Extra = [0, "", False]
        Offset = 0
    if isinstance(Row[Offset], type(None)):
        Row[Offset] = ""
    if isinstance(Row[Offset + 1], type(None)):
        Row[Offset + 1] = ""
    Row[Offset + 2] = Timestamp.Ago(Row[Offset + 2])

    if Row[Offset] != "":
        ToolTip = GLib.markup_escape_text(Row[Offset])
    if ToolTip != "" and Row[Offset + 1] != "":
        ToolTip += " - "
    ToolTip += GLib.markup_escape_text(Row[Offset + 1])

    Store.append((EmptyCopy, *Extra, *Row, ToolTip))
    Path = Gtk.TreePath.new_from_indices([len(Store) - 1])
    Image.FillPixbuf(
        Row[-2],
        EmptyCopy,
        Store.row_changed,
        Path,
        Store.get_iter(Path),
    )
    MainSpace.show_all()


def GridUpdate(
    Row,
    EmptyCopy,
    MainSpace,
    List,
    Settings,
    Publicationer,
    Grid,
    AddPage,
    Stater,
):
    BoxBuilder = Gtk.Builder.new_from_file(Places.GladeDir + "Box.glade")
    Thumbnail = BoxBuilder.get_object("Thumbnail")
    Thumbnail.set_from_pixbuf(EmptyCopy)
    Image.FillPixbuf(Row[-2], EmptyCopy, Thumbnail.set_from_pixbuf, EmptyCopy)

    Extra, HiddenExtra = [0, "", False], True
    if List == "Wallet":
        Extra = Row
        HiddenExtra = False
    else:
        Extra.extend(Row)
    Boxer = Box(
        BoxBuilder,
        EmptyCopy,
        Extra,
        Publicationer,
        int(Settings["GridTitleRows"]),
        int(Settings["GridAuthorRows"]),
        int(Settings["GridPadding"]),
        HiddenExtra,
        AddPage,
        Stater,
    )
    BoxBuilder.connect_signals(Boxer)
    PubBox = BoxBuilder.get_object("Box")
    Grid.add(PubBox)
    FlowChild = PubBox.get_parent()
    StyleContext = FlowChild.get_style_context()
    StyleContext.add_provider(
        CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )
    FlowChild.set_halign(Gtk.Align.CENTER)
    MainSpace.show_all()


def ListContent(Height, Width, MainSpace, Queue, *args):
    Queue.put(MainSpace.get_allocated_height() // Height + 1)


def GridContent(
    Height, Width, MainSpace, Queue, Settings, List, CheckHeight, TextHeight
):
    Lines = int(Settings["GridTitleRows"] + Settings["GridAuthorRows"] + 1)
    Extra = 0
    if List == "Wallet":
        Lines += 4
        Extra = CheckHeight
    # TODO: How to know - 2
    HorizontalContent = (MainSpace.get_allocated_width() - 2) // (
        Width + 2 * int(Settings["GridPadding"])
    )
    VerticalContent = (MainSpace.get_allocated_height() - 2) // (
        Height + 2 * int(Settings["GridPadding"]) + TextHeight * Lines + Extra
    ) + 1
    Queue.put(HorizontalContent * VerticalContent + 1)
