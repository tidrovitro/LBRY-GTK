# Welcome to LBRY-GTK

## Introduction

<hr/>

This is the LBRY-GTK Help article, that will try to guide users around the GUI. Keep in mind, that this program is not yet 100% functional, so there are nonfunctional buttons and missing features. When this article describes something, that is not yet implemented, it will have (NYI) after.

## The graphical user interface

<hr/>

![](./01.png)

The window supports dropping lbry links and links from lbry sites. The structure of the window consists of 4 main parts:

  - SidePanel
  - TopPanel
  - PageBar
  - WorkSpace

The program also has a system tray icon, that hides the window on left click. It has the following entries:

  - Show/Hide - Show or Hide the window, depending on its current state.
  - Quit - Quit the program.

### SidePanel

<hr/>

The sidepanel has many pre-defined lists that are good starting points in using the program:

  - Home - A customizable page, that can be anything, not only a list.
  - Following - Publications from channels you are following.
  - Your Tags - Publications with tags you are interested in.
  - Discover - New, trending publications.
  - Library - Publications that are cached by LBRYNet.
  - Collections - Collections of publications made by you.
  - Followed - Channels you are following.
  - Uploads - Uploads made by you.
  - Channels - Your channels.

### PageBar

<hr/>

The pagebar has controls related to the paging system:

  - Reload - Reloads the current page.
  - Page - Switches to the page.
  - Close - Closes the page.
  - New Page - Opens a new page with Home.

### TopPanel

<hr/>

The toppanel has every control of the program:

  - Back - Goes back a page, shows previous pages on right click.
  - Forward - Goes forward a page, shows next pages on right click.
  - LBRY-GTK - Inserts lbry:// into the search bar, and enters it.
  - Search bar - Search publications, or resolve them, if they start with lbry://.
  - New - Create content, check out [Uploading](./Construction.md) for more.
     - Publication - Create a publication.
     - Channel - Create a Channel. (NYI)
  - \* - The name will change.
     - Settings - Change program settings, check out [Settings](./Construction.md) for more.
     - Help - Open this article.
     - Status - Shows the status of different components.
     - About - Check license and info about the author.
  - Balance - Check your balance, with subcategories and your transaction history.
  - Account - (NYI)

For info about the publication page, check out [Publication](./Construction.md).

For info about commenting, check out [Commenting](./Construction.md).

For info about markdown, check out [Markdown](./Markdown.md).
