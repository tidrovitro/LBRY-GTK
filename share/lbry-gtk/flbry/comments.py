################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# This file will perform a simple search on the LBRY network.

import json, requests

from flbry import url, channel, parse, settings, error


def sign(channel_name, data, server="http://localhost:5279"):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "channel_sign",
                "params": {
                    "channel_name": channel_name,
                    "hexdata": data.encode("utf-8").hex(),
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return [json_data["signature"], json_data["signing_ts"]]


def setting_get(channel_id, comment_server):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.React",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "setting.Get",
                "params": {"channel_id": channel_id},
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def reaction_react(
    channel_name,
    channel_id,
    comment_server,
    comment_ids,
    reaction_type,
    remove=False,
    server="http://localhost:5279",
):
    signage = sign(channel_name, channel_name, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    clear_types = ""

    if not remove:
        if reaction_type == "like":
            clear_types = "dislike"
        elif reaction_type == "dislike":
            clear_types = "like"

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "reaction.React",
        "params": {
            "channel_name": channel_name,
            "channel_id": channel_id,
            "comment_ids": comment_ids,
            "type": reaction_type,
            "signature": signature,
            "signing_ts": signing_ts,
        },
    }

    if remove:
        json["params"]["remove"] = remove
    elif clear_types != "":
        json["params"]["clear_types"] = clear_types

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.React", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def reaction_list(
    channel_name,
    channel_id,
    comment_server,
    comment_ids,
    server="http://localhost:5279",
):

    signage = sign(channel_name, channel_name, server)

    signature, signing_ts = "", ""

    if not isinstance(signage, str):
        signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=reaction.List",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "reaction.List",
                "params": {
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "comment_ids": comment_ids,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def list(
    claim_id,
    comment_server,
    channel_name="",
    channel_id="",
    parent_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    # This function will list a list of comments of a certain claim_id
    # It will preview a very basic form of comment. You will have
    # to select a comment to interact with it further. Like read the
    # whole text, if it not short ( or in a markdown format ). Or
    # do other things like replies.

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "comment.List",
        "params": {
            "claim_id": claim_id,
            "page_size": page_size,
            "page": page,
            "sort_by": 3,
            "top_level": True,
        },
    }

    if channel_name != "":
        json["params"]["channel_name"] = channel_name

    if channel_id != "":
        json["params"]["channel_id"] = channel_id

    if parent_id != "":
        json["params"]["parent_id"] = parent_id
        json["params"]["top_level"] = False

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.List", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return [parse.comments(json_data, server=server), json_data["total_pages"]]


def post(
    claim_id,
    comment,
    channel_name,
    channel_id,
    comment_server,
    parent_id="",
    support_tx_id="",
    server="http://localhost:5279",
):

    # This will post a comment under either a publication or a
    # comment as a reply.

    signage = sign(channel_name, comment, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    json = {
        "id": 1,
        "jsonrpc": "2.0",
        "method": "comment.Create",
        "params": {
            "comment": comment,
            "channel_id": channel_id,
            "channel_name": channel_name,
            "claim_id": claim_id,
            "signature": signature,
            "signing_ts": signing_ts,
        },
    }

    if parent_id != "":
        json["params"]["parent_id"] = parent_id

    if support_tx_id != "":
        json["params"]["support_tx_id"] = support_tx_id

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Create", json=json
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def update(
    comment,
    comment_id,
    channel_name,
    channel_id,
    comment_server,
    server="http://localhost:5279",
):

    signage = sign(channel_name, comment, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Edit",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "comment.Edit",
                "params": {
                    "comment_id": comment_id,
                    "comment": comment,
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def delete(
    comment_id,
    channel_name,
    channel_id,
    comment_server,
    server="http://localhost:5279",
):
    signage = sign(channel_name, comment_id, server)

    if isinstance(signage, str):
        return signage

    signature, signing_ts = signage

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            comment_server + "?m=comment.Abandon",
            json={
                "id": 1,
                "jsonrpc": "2.0",
                "method": "comment.Abandon",
                "params": {
                    "channel_name": channel_name,
                    "channel_id": channel_id,
                    "comment_id": comment_id,
                    "signature": signature,
                    "signing_ts": signing_ts,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data
