################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import json, requests, queue, threading

from flbry import url, channel, parse, settings, error


def get_type(meta_server, auth_token, notification_type, type_queue):
    try:
        json_data = {"error": {}}
        json_data = requests.get(
            meta_server + "/notification/list",
            params={"auth_token": auth_token, "type": notification_type},
        ).json()
        json_data = json_data["data"]
    except Exception as e:
        type_queue.put(error.error(e, json_data["error"]))
    type_queue.put(json_data)


notification_types = [
    "comments",
    "hyperchats",
    "comment_replies",
    "hyperchat_replies",
    "new_content",
    "livestream",
]


def sorter(element):
    return element["id"]


def list(meta_server, auth_token="", raw=False):
    # Get notifications
    error = ""

    all_data = []
    queues = []

    for notification_type in notification_types:
        queues.append(queue.Queue())
        args = [meta_server, auth_token, notification_type, queues[-1]]
        threading.Thread(None, get_type, None, args).start()

    for type_queue in queues:
        data = type_queue.get()
        if isinstance(data, str):
            error = data
        elif isinstance(data, type([])):
            all_data.extend(data)

    all_data.sort(key=sorter, reverse=True)

    if all_data == [] and error != "":
        return error
    if raw:
        return all_data
    return parse.notifications(all_data)
