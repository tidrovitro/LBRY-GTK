################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import requests

from flbry import parse, error


def list(
    resolve=False,
    resolve_claims=False,
    account_id="",
    wallet_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    json = {
        "method": "collection_list",
        "params": {"page_size": page_size, "page": page},
    }

    if resolve_claims != False:
        json["params"]["resolve_claims"] = resolve_claims

    if account_id != "":
        json["params"]["account_id"] = account_id

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    if resolve != False:
        json["params"]["resolve"] = resolve

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.publications(json_data, server=server)


def resolve(
    claim_id="",
    url="",
    wallet_id="",
    page_size=5,
    page=1,
    server="http://localhost:5279",
):

    json = {
        "method": "collection_resolve",
        "params": {"page_size": page_size, "page": page},
    }

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    if claim_id != "":
        json["params"]["claim_id"] = claim_id

    if url != "":
        json["params"]["url"] = url

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.publications(json_data, server=server)
