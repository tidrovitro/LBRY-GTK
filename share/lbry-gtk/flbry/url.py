#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will fetch an LBRY URL directly and print out various
# options that the user may do with the publication.

import requests
import json, os, urllib.parse
from flbry import channel, search, wallet, error


def get(urls=[], do_search=True, server="http://localhost:5279"):
    # Allows user to interact with the given URL

    data = []
    claim_dict = {}
    url_dict = {}

    for index in range(len(urls)):
        # Decode the URL to turn percent encoding into normal characters
        # Example: "%C3%A9" turns into "é"

        urls[index] = urllib.parse.unquote(urls[index])

        ##### Converting an HTTPS domain ( of any website ) into lbry url ###

        # Any variation of this:
        # https://odysee.com/@blenderdumbass:f/hacking-fastlbry-live-with-you:6
        # customlbry.com/@blenderdumbass:f/hacking-fastlbry-live-with-you:6

        # Should become this:
        # lbry://@blenderdumbass:f/hacking-fastlbry-live-with-you:6

        if (
            "/" in urls[index]
            and not urls[index].startswith("@")
            and not urls[index].startswith("lbry://")
        ):
            if "@" in urls[index]:
                urls[index] = urls[index][urls[index].find("@") :]
            else:
                urls[index] = urls[index][urls[index].rfind("/") - 1 :]

            # Some web interfaces pass data through URLs using a '?'.
            # This is not valid for a LBRY URL, so we remove everything
            # after it to avoid errors.
            urls[index] = urls[index].split("?")[0]

            urls[index] = "lbry://" + urls[index]

        # If a url looks like it might be a claim id, try to get the claim from it
        if len(urls[index]) == 40 and not urls[index].startswith("lbry://"):
            claim_dict[index] = urls[index]
        else:
            url_dict[index] = urls[index]

    if claim_dict:

        claim_list = []

        for index in claim_dict:
            claim_list.append(claim_dict[index])

        try:
            json_claim_data = {"error": {}}
            json_claim_data = requests.post(
                server,
                json={
                    "method": "claim_search",
                    "params": {"claim_ids": claim_list},
                },
            ).json()
            json_claim_data = json_claim_data["result"]
        except Exception as e:
            return error.error(e, json_claim_data["error"])

        claim_data = json_claim_data["items"]

    if url_dict:

        url_list = []

        for index in url_dict:
            url_list.append(url_dict[index])

        try:
            json_url_data = {"error": {}}
            json_url_data = requests.post(
                server,
                json={"method": "resolve", "params": {"urls": url_list}},
            ).json()
            json_url_data = json_url_data["result"]
        except Exception as e:
            return error.error(e, json_url_data["error"])

        url_data = []

        for item in url_list:
            to_add = json_url_data[item]
            if "error" in json_url_data[item]:
                e = "Url error"
                to_add = error.error(e, json_url_data[item]["error"])
            url_data.append(to_add)

    for index in range(len(urls)):
        if index in url_dict.keys():
            data.append(url_data.pop(0))
        else:
            data.append(claim_data.pop(0))

        if not isinstance(data[index], str):

            ### REPOST ####

            # Sometimes a user wants to select a repost. This will not
            # load anything of a value. A repost is an empty blob that
            # links to another blob. So I want to automatically load
            # the actuall publication it self here.

            if (
                "value_type" in data[index]
                and data[index]["value_type"] == "repost"
            ):
                data[index] = get(
                    [data[index]["reposted_claim"]["canonical_url"]],
                    server=server,
                )[0]

            #### FORCE SEARCH ###

            # Sometimes user might type something that is not a url
            # in this case ["value_type"] will not be loaded. And in
            # this case we can load search instead.

            if "value_type" not in data[index] and do_search:
                data[index] = search.simple(urls[index])

            # Now that we know that don't search for it. We can make
            # one thing less broken. Sometimes a user might type a
            # urls that's going to be resolved but that doesn't have
            # the lbry:// in the beginning of it. Like typing
            # @blenderdumbass instead of lbry://blenderdumbass

            # I want to add the lbry:// to it anyway. So non of the
            # stuff later will break.

            if not urls[index].startswith("lbry://") and not urls[
                index
            ].startswith("@"):
                urls[index] = "lbry://" + urls[index]
    return data
    """
        elif c.startswith("repost"):

            name = input("   Name (enter for name of publication): ")
            if not name:
                name = out["normalized_name"]

            a = c.split()
            try:
                bid = a[1]
            except:
                bid = input("   Bid: ")

            claim_id = out["claim_id"]
            ch, chid = channel.select("Channel to repost to:", True)

            repost_out = check_output([lbrynet_binary["b"], "stream", "repost", "--name="+name, "--bid="+bid, "--claim_id="+claim_id, "--channel_id="+chid])
            repost_out = json.loads(repost_out)

            if "message" in repost_out:
                center("Error reposting: "+repost_out["message"], "bdrd")
            else:
                center("Successfully reposted to "+ch, "bdgr")
    """


# List of web instances of LBRY
web_instances = [
    # NAME     URL                    MAINTAINER   INTERFACE
    ["Odysee", "https://odysee.com/", "LBRY Inc.", "JavaScript"],
    ["Madiator", "https://madiator.com/", "Madiator2011", "JavaScript"],
    # TODO: Add librarian-instance option. Importing flbry.settings from here gives an error.
    [
        "Librarian",
        "https://librarian.bcow.xyz/",
        "imabritishcow",
        "Invidous-like web interface",
    ],
]


def web(publication_url):
    # Some web clients don't work with '#' in the URL
    web = publication_url.replace("#", ":")

    web_list = []
    for instance in web_instances:
        web_list.append([instance[0], web.replace("lbry://", instance[1])])

    return web_list
