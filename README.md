# <img src="./share/icons/hicolor/scalable/apps/lbry-gtk.svg" width="10%" height="auto"> LBRY-GTK

LBRY-GTK as the name may suggest is a GTK client to the LBRY network. LBRY-GTK in its early stages was a fork of FastLBRY-Terminal, however the code was modified a lot in order to become a full GTK GUI app. LBRY-GTK is also:
* A desktop (not webbrowser) app for doing a variety of things on the LBRY network
* Cross platform (For now Windows & Linux only, contributions welcome)
* Feature rich (Please make issues or talk in the matrix chat if something is missing)
* FOSS (Free as in Freedom) software

## Contribution:

LBRY-GTK has a lot of feature and bugfix ideas. All of these are listed in the [TODO.md file](TODO.md). Any help in terms of development or suggestions of what can be added/fixed is very much appreciated. For contributing code, check out the [CONTRIBUTING.md file](CONTRIBUTING.md).

## Links:

Questions and chat about the project: [Matrix](https://matrix.to/#/#LBRY-GTK:matrix.org)

AUR package: [lbry-gtk-git](https://aur.archlinux.org/packages/lbry-gtk-git)

Based on: [FastLBRY-terminal](https://notabug.org/jyamihud/FastLBRY-terminal)

Other great LBRY projects: [Awesome-LBRY](https://github.com/LBRYFoundation/Awesome-LBRY)

Download can be found in: [Releases](https://codeberg.org/MorsMortium/LBRY-GTK/releases)

## Features:

For a list of features check out the [FEATURES.md file](FEATURES.md).

## Screenshot Overview:

![](./Images/12.png)
![](./Images/13.png)