#!/usr/bin/env sh

################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################


# Making build path for all the clutter and AppImage directory
mkdir -p build && cd build
mkdir -p lbry-gtk.AppDir

# Get some tools, dependencies
wget -nc "https://github.com/AppImage/AppImageKit/releases/download/12/appimagetool-x86_64.AppImage"
wget -nc "https://github.com/lbryio/lbry-sdk/releases/download/v0.109.0/lbrynet-linux.zip"
git clone "https://invent.kde.org/brauch/appimage-exec-wrapper.git"

# Make required setups for dependencies/tools
chmod +x appimagetool-x86_64.AppImage
unzip lbrynet-linux.zip
cd appimage-exec-wrapper
make
cd ..

# Script for running appimage
cat > AppRun <<EOF
#!/usr/bin/env sh

# Needs to be APPDIR, otherwise external programs won't work
APPDIR=\$(dirname "\$0")
export PATH="\$PATH":"\$APPDIR"/lbry-gtk
export APPIMAGE_ORIGINAL_LD_LIBRARY_PATH=\$LD_LIBRARY_PATH
export PYTHONPATH="\$APPDIR"/share/lbry-gtk/"\${PYTHONPATH:+:\$PYTHONPATH}"
export LD_LIBRARY_PATH="\$APPDIR"/lbry-gtk
export APPIMAGE_STARTUP_LD_LIBRARY_PATH=\$LD_LIBRARY_PATH
export LD_PRELOAD="\$APPDIR"/lbry-gtk/wrapper.so

"\$APPDIR"/lbry-gtk/lbry-gtk
EOF

chmod +x AppRun

# Initial installation
pyinstaller --windowed --noconfirm --hidden-import cairo \
	--exclude-module Source ./../../bin/lbry-gtk

# Copying and moving required libraries
cp -r ../../share/lbry-gtk/* ./dist/lbry-gtk
mv ./lbrynet ./dist/lbry-gtk
mv ./appimage-exec-wrapper/exec.so ./dist/lbry-gtk/wrapper.so
cp ../../lbry-gtk.desktop ./dist
mv AppRun ./dist
cp ../../share/icons/hicolor/scalable/apps/lbry-gtk.svg ./dist
cp -r ../../share ./dist 
cp -r ../../share/* ./dist/lbry-gtk/share

# Removing unnecessary stuff (to keep size small)
cd ./dist/lbry-gtk/share/icons
ls --ignore=Adwaita --ignore=hicolor | xargs rm -rf
cd ../../../..

find ./dist/lbry-gtk/share/icons/Adwaita/ -type f -not \
    \( -name 'image-missing*' -or -name 'go-next*' \
    -or -name 'go-previous*' -or -name 'list-remove*' \
    -or -name 'list-add*'  -or -name 'pan*' \
    -or -name 'view-refresh*' -or -name 'window-close*' \
    -or -name 'media-playback-start*' -or -name 'dialog-password*' \
    -or -name 'menu_new*' -or -name 'preferences-other*' \
    -or -name 'image-loading*' -or -name 'document-properties*' \
    -or -name 'applications-others*' -or -path '*places/*' \) -delete

rm -rf ./dist/lbry-gtk/share/locale

# Copy everything to AppDir and make AppImage
cp -r dist/* ./lbry-gtk.AppDir

# Deploy
ARCH=x86_64 ./appimagetool-x86_64.AppImage ./lbry-gtk.AppDir
