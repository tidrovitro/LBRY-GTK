# Rules of Contributing:

- Use PascalCase in Source, snake_case in flbry.
- When working on the GUI, use the Glade files as much as possible.
- When creating containers, use GtkBox instead of GtkGrid as much as possible.
- Run your code through black formatter, before commit. (black -l 80 ./)
- When all work is done, squash your commits, before merge.

## Squashing commits:

When making commits, remember to squash your commits.
You can squash starting with command:

  ```git reset --soft HEAD~n```

Where *n* is the number of commits you had already made. After this you should commit your changes e.g. ```git commit -a``` to add all changed files.

You can then push your commits to remote repository. If you already had pushed commits to your repository, you can issue this command to force update the repository:

  ```git push origin +main```

Where '+'-sign forces the repository to take the commits and overwrite any past commits that aren't present in the local version.

**WARNING**: Do not reset more than what you commited, otherwise you might replace commits made by other contributors. Make sure you know exactly how many commits you made before resetting.
