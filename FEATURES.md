# List of features:
  * Lists, with thumbnails
    * Search
    * Advanced Search
    * Following
    * Your Tags
    * Discover
    * Library
    * Collections
    * Followed
    * Uploads
    * Channels
  * Wallet
    * Statistics
    * History
  * Open publication pages
    * Channel
    * Video
    * Audio
    * Article
    * Image
    * Collection
  * Open publications in browser
    * [Odysee](https://odysee.com/)
    * [Madiator](https://madiator.com/)
    * [Librarian](https://librarian.bcow.xyz/)
  * Comment as any of your channels (with or without tip)
    * Open (with markdown support)
    * Write
    * Edit
    * Delete
    * Reply to
    * React to
  * Channel
    * Follow
    * Unfollow
    * Get RSS links
      * [Odysee](https://odysee.com/)
      * [Librarian](https://librarian.bcow.xyz/)
  * Make publications
    * Video
    * Audio
    * Article
    * Image
  * Search tags from publication page
  * Go back and forth (with history)
  * Display style
    * Grid
    * List
  * Extensive settings system
  * Specify commands per stream type
    * Video
    * Audio
    * Image
    * Document
  * Advanced Search
    * Text
    * Channels
    * Claims
    * Date
      * Simple
      * Custom
    * Claim Type
    * Stream Type
    * Tags
      * Any
      * All
      * Not
  * Order by
    * Release Time
    * Name
    * Trending
  * Order Direction
    * Descending
    * Ascending
  * Markdown display (set Document Command to LBRY-GTK-Document)
  * Get related content to publication
  * Tip or boost publication
  * Get Odysee notifications
  * Control the app with keyboard (except Advanced Search, New Publication and Settings)
  * Customizable Home page
  * Hide highly disliked comments
